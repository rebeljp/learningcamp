let mix = require("laravel-mix");
let webpack = require("webpack"); //to access built-in plugins

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
if (mix.inProduction()) {
	mix.version();
	mix.options({
		uglify: {
			parallel: true,
			cache: true,
			sourceMap: true
		}, // Uglify-specific options. https://webpack.github.io/docs/list-of-plugins.html#uglifyjsplugin
	});
	mix.webpackConfig({
		devtool: "source-map",
		node: {
			fs: "empty"
		},
		resolve: {
			alias: {
				vue$: "vue/dist/vue.runtime.common.js"
			}
		},
		module: {
			rules: [
				{ parser: { amd: false } }
			]
		},
		plugins: [
			new webpack.NoEmitOnErrorsPlugin(),
		]
	});
} else {
	mix.webpackConfig({
		devtool: "source-map",
		module: {
			rules: [
				{ parser: { amd: false } }
			]
		},
		node: {
			fs: "empty"
		},
	});
}

mix.autoload({
	jquery: ["$", "window.jQuery", "jQuery", "window.$", "jquery", "window.jquery"],
})
	.js("resources/assets/js/app.js", "public/js")
	.js("resources/assets/js/admin.js", "public/js")
	.js("resources/assets/js/components/SectionHome.js", "public/js")
	.js("resources/assets/js/components/SectionContact.js", "public/js")
	.sass("resources/assets/sass/app.scss", "public/css")
	.sourceMaps();

//mix.copyDirectory("resources/css_js/fonts", "public/fonts");

mix.styles([
	"node_modules/@fortawesome/fontawesome-svg-core/styles.css",
	"resources/css_js/owl.carousel.css",
	"resources/css_js/owl.theme.css",
	"node_modules/magnific-popup/dist/magnific-popup.css",
	"node_modules/slick-carousel/slick/slick.css",
	"node_modules/slick-carousel/slick/slick-theme.css",
	"resources/css_js/meanmenu.css",
], "public/css/all.css");

mix.scripts([
	"node_modules/magnific-popup/dist/jquery.magnific-popup.min.js",
	"node_modules/slick-carousel/slick/slick.min.js",
	"resources/css_js/jquery.meanmenu.min.js",
	"node_modules/sharer.js/sharer.min.js",
], "public/js/all.js");

mix.styles([
	"node_modules/tempusdominus-bootstrap-4/build/css/tempusdominus-bootstrap-4.min.css",
	"node_modules/inscrybmde/dist/inscrybmde.min.css",
	"node_modules/bootstrap4-tagsinput-douglasanpa/tagsinput.css",
], "public/css/editor.css");

mix.scripts([
	"node_modules/moment/min/moment-with-locales.min.js",
	"node_modules/tempusdominus-bootstrap-4/build/js/tempusdominus-bootstrap-4.min.js",
	"node_modules/inscrybmde/dist/inscrybmde.min.js",
	"node_modules/bootstrap4-tagsinput-douglasanpa/tagsinput.min.js"
], "public/js/editor.js");

mix.styles([
	"resources/css_js/custom.css"
], "public/css/cls.css");

mix.scripts([
	"resources/css_js/custom.js"
], "public/js/cls.js");

