module.exports = {
	testRegex: "tests/.*.spec.js$",
	moduleFileExtensions: [
		"js",
		"json",
		"vue"
	],
	"transform": {
		"^.+\\.js$": "<rootDir>/node_modules/babel-jest",
		".*\\.(vue)$": "<rootDir>/node_modules/vue-jest"
	},
	// support the same @ -> src alias mapping in source code
	"moduleNameMapper": {
		"^@/(.*)$": "<rootDir>/resources/assets/js/$1"
	}
};
