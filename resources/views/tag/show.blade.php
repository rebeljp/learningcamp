@extends('layouts.plane')
@section('pageType', 'event-01')
@section('body')

<!-- Teachers Area section -->
<section class="events-list-03">
    <div class="container">
        <div class="row">
            <div class="card-columns">
                @foreach ($tags as $product)
                    @include('sections.card', [$product, 'action' => $product['action']])
                @endforeach
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="event-bottom-btn align-middle text-center">
                    {{-- {{ $tags->links() }} --}}
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ./ End Teachers Area section -->

@endsection
