@component('mail::message')
# Introduction

Hello

Terima kasih telah melakukan registrasi di website kami,

@component('mail::button', ['url' => 'https://laravel.com/docs/5.6/notifications#writing-the-message'])
Button Text
@endcomponent

Regards,<br>
{{ config('app.name') }}
@endcomponent
