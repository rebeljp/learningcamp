@extends('layouts.plane')
@section('slider')

<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img class="d-block w-100 lazyload"
            data-sizes="auto"
            data-src="https://res.cloudinary.com/cls-web-id/image/upload/c_fit,w_1024/v1524491346/images/Header_201679x790.jpg"
            data-srcset="https://res.cloudinary.com/cls-web-id/image/upload/c_fit,w_480/v1524491346/images/Header_201679x790.jpg 300w,
            https://res.cloudinary.com/cls-web-id/image/upload/c_fit,w_800/v1524491346/images/Header_201679x790.jpg 600w,
            https://res.cloudinary.com/cls-web-id/image/upload/c_fit,w_1024/v1524491346/images/Header_201679x790.jpg 900w,
            https://res.cloudinary.com/cls-web-id/image/upload/c_fit,w_1679/v1524491346/images/Header_201679x790.jpg 1200w"
            src="https://res.cloudinary.com/cls-web-id/image/upload/c_fit,h_815,w_1679,q_auto:low,e_blur:1000/v1524491346/images/Header_201679x790.jpg"
            alt="header-1">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100 lazyload"
            data-sizes="auto"
            data-src="https://res.cloudinary.com/cls-web-id/image/upload/c_fit,w_1024/v1524491346/images/Header_201680x815.jpg"
            data-srcset="https://res.cloudinary.com/cls-web-id/image/upload/c_fit,w_480/v1524491346/images/Header_201680x815.jpg 300w,
            https://res.cloudinary.com/cls-web-id/image/upload/c_fit,w_800/v1524491346/images/Header_201680x815.jpg 600w,
            https://res.cloudinary.com/cls-web-id/image/upload/c_fit,w_1024/v1524491346/images/Header_201680x815.jpg 900w,
            https://res.cloudinary.com/cls-web-id/image/upload/c_fit,w_1679/v1524491346/images/Header_201680x815.jpg 1200w"
            src="https://res.cloudinary.com/cls-web-id/image/upload/c_fit,h_815,w_1679,q_auto:low,e_blur:1000/v1524491346/images/Header_201680x815.jpg"
            src="https://res.cloudinary.com/cls-web-id/image/upload/c_fit,h_815,w_1679/v1535125587/images/Header_201680x815.jpg"
            alt="header-2">
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
@endsection

@section('pageType', 'home_version_02')
@section('body')
<!-- <div id="section-welcome">
    <section-welcome></section-welcome>
</div> -->
@includeWhen($classes, 'sections.class', $classes)
<div id="section-video">
    <section-video :href="'{{ $videoUrl }}'"></section-video>
</div>
@includeWhen($events, 'sections.events', $events)
<!-- <div id="section-parents">
    <section-parents></section-parents>
</div> -->
@endsection
@push('style')
@endpush
@push('scripts')
<script  src="{{ mix('/js/SectionHome.js') }}"></script>
<script nonce="{{ Bepsvpt\SecureHeaders\SecureHeaders::nonce() }}">
$(document).ready(function(){
    "use strict";
    $(".carousel").carousel();
});
</script>
@endpush
