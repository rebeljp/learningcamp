@extends('layouts.plane')
@section('pageType', 'contact')
@section('body')

Co-Learning Space (CLS) is an online hub and a community that has goal to bring together people that have the same spirit and interest to study together from any open educational resources available. Through our platform, we hope we could help as many people to learn by conducting group discussion and online discussion. CLS was founded under the belief that every people should have the right to learn. We also believe that learning is a life time process. Let’s not stop learning and learn together!

@endsection
