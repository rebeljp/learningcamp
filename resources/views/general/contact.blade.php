@extends('layouts.plane')
@section('pageType', 'contact')
@section('body')

<div id="section-contact">
    <section-contact></section-contact>
</div>

@endsection

@push('scripts')
<script  src="{{ mix('/js/SectionContact.js') }}"></script>
@endpush
