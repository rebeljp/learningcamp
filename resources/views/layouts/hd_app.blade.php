<nav class="navbar navbar-expand-lg sticky-top navbar-dark bg-dark">
    <a href="/" class="navbar-brand">
        <img data-src="https://res.cloudinary.com/cls-web-id/image/upload/c_scale,q_100:420,w_40/v1524491336/images/CLS" src="https://res.cloudinary.com/cls-web-id/image/upload/c_scale,e_blur:1000,h_40,q_auto:low,w_40/v1524491336/images/CLS.png" alt="Co-LearningSpace.web.id" title="CLS" class="d-inline-block align-top lazyload">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
        aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse " id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
            <li class="nav-item">
                <a data-scroll href="{{url('class')}}" class="nav-link active">Class</a>
            </li>
            <li class="nav-item">
                <a data-scroll href="{{url('event')}}" class="nav-link active">Event</a>
            </li>
            <li class="nav-item">
                <a data-scroll href="{{url('contact-us')}}" class="nav-link active">@lang('common.contact')</a>
            </li>
        </ul>
        <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
            @guest
            <li class="nav-item dropdown d-none d-lg-block">
                <a class="nav-link dropdown-toggle active" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Sign In</a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown01">
                    <form class="px-4 py-3 eduread-register-form" action="{{ route('login') . '?previous=' . Request::fullUrl() }}" method="POST" >
                        @csrf
                        <div class="form-group">
                            <input type="email" autocomplete="username" placeholder="Email" name="email" id="email" class="form-control" placeholder="email@example.com">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" autocomplete="current-password" placeholder="Password" name="password" id="password" placeholder="Password">
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="remember" name="remember">
                            <label class="form-check-label" for="remember">
                              @lang('auth.login.remember')
                            </label>
                        </div>
                        <div class="form-group register-btn">
                            <button type="submit" class="btn btn-cls btn-block ">Sign in</button>
                        </div>
                    </form>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="{{ route('password.request') }}">@lang('auth.login.forgot')</a>
                    <a class="dropdown-item" href="{{ url('register') }}">@lang('auth.login.member') Sign up</a>
                </div>
            </li>

            <li class="nav-item d-block d-lg-none">
                <a data-scroll href="{{url('login')}}" class="nav-link active">Sign In</a>
            </li>
            @endguest
            @auth
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle active" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Hi {{Auth::user()->name}}</a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown01">
                    <a href="{{url('user/' . Auth::user()->hashed_id . '/profile')}}" class="dropdown-item">
                        <i aria-hidden class="fas fa-user fa-fw"></i><span> Profile</span>
                    </a>
                    <a href="{{url('user/' . Auth::user()->hashed_id . '/ubah')}}" class="dropdown-item">
                        <i aria-hidden class="fas fa-sliders-h fa-fw"></i><span> User Setting</span>
                    </a>
                    @can('admin', App\User::class)
                    <div class="dropdown-divider"></div>
                    <a href="{{url('admin')}}" class="dropdown-item">
                        <i aria-hidden class="fas fa-cog fa-fw"></i><span> Admin Panel</span></a>
                    @endcan
                    <div class="dropdown-divider"></div>
                    <a href="{{url('logout')}}" class="dropdown-item"><i aria-hidden class="fas fa-sign-out-alt fa-fw"></i><span> Sign out</span></a>
                </div>
            </li>
            @endauth
        </ul>
    </div>
</nav>
<header>
    <div class="header-body">

    </div>
</header>
@yield('slider')
