<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="application-name" content="Co-Learning Space" />
	<meta name="apple-mobile-web-app-title" content="Co-Learning Space" />
	<meta name="author" content="Co-Learning Space" />
	<meta name="APP_HASH" content="{{env('MIX_APP_HASH')}}" />
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta http-equiv="Cache-control" content="no-cache">
	<meta http-equiv="Expires" content="-1">
    {!! SEO::generate(true) !!}
    <link href="https://res.cloudinary.com" rel="preconnect">
    <link href="https://maps.googleapis.com" rel="preconnect">
    <link href="https://www.googletagmanager.com" rel="preconnect">
    <link href="https://widget.cloudinary.com" rel="preconnect">
    <link href="https://www.google.co.id" rel="preconnect">
    <link href="https://www.facebook.com" rel="preconnect">
    <link href="https://connect.facebook.net" rel="preconnect">
    <link href="https://staticxx.facebook.com" rel="preconnect">
    <link href="https://cls-id.disqus.com" rel="preconnect">
    <link href="https://platform.twitter.com" rel="preconnect">
    <link href="https://sentry.io" rel="preconnect">
    <link href="https://pagead2.googlesyndication.com" rel="preconnect">

	<link href="{{ mix('/css/app.css') }}" rel="stylesheet preconnect preload" as="style" />
	<link href="{{ mix('/css/all.css') }}" rel="stylesheet preconnect preload" as="style" />
    @stack('style')
    <link href="{{ mix('/css/cls.css') }}" rel="stylesheet preconnect preload" as="style" />
    @include('googletagmanager::head')

    <link rel="alternate" hreflang="x-default" href="/" />
    <meta content="notranslate" name="google">

	<link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
	<link rel="img_src" href="/android-icon-192x192.png">
	<link rel="shortcut icon" href="/favicon.ico">
	<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
	<link rel="manifest" href="/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">

    <!-- Base URL to use for all relative URLs contained within the document -->
	<base href="/">
</head>

<body class="@yield('pageType')">
    @include('googletagmanager::body')
    @yield('body')
	@include('layouts.ft_app')
</body>

</html>
