<!-- Footer Area section -->
<footer>
    <div class="container">
        <div class="col-sm-12 footer-content-box">
            <div class="row">
                <div class="col-sm-4">
                    <h3>
                        <span>
							<img data-src="https://res.cloudinary.com/cls-web-id/image/upload/c_scale,q_100:420,w_40/v1524491336/images/CLS" src="https://res.cloudinary.com/cls-web-id/image/upload/c_scale,e_blur:1000,h_40,q_auto:low,w_40/v1524491336/images/CLS.png" alt="Co-LearningSpace.web.id" title="Co-Learning Space" class="lazyload"/>
						</span> Co-Learning Space
                    </h3>
                    <p>Find your study buddy, whom you can learn something new with.</p>
                    <ul class="fa-ul">
                        <li><span><i aria-hidden class="fas fa-envelope footer-icon"></i></span>cs@co-learningspace.web.id</li>
                    </ul>
                </div>

                <div class="col-sm-4">
                    <h3>CLS</h3>
                    <ul class="fa-ul">
                        <li><a href="{{url('contact-us')}}"><span><i aria-hidden class="fas fa-long-arrow-alt-right footer-icon"></i></span>@lang('common.contact')</a></li>
                        <li><a href="{{url('about-us')}}"><span><i aria-hidden class="fas fa-long-arrow-alt-right footer-icon"></i></span>@lang('common.about')</a></li>
                        <li><a href="{{url('privacy-policy')}}"><span><i aria-hidden class="fas fa-long-arrow-alt-right footer-icon"></i></span>@lang('common.privacy')</a></li>
                        <li><a href="{{url('terms-of-use')}}"><span><i aria-hidden class="fas fa-long-arrow-alt-right footer-icon"></i></span>@lang('common.terms')</a></li>

                    </ul>
                </div>

                <div class="col-sm-4">
                    @guest
                    <h3>get in touch</h3>
                    <p>@lang('common.subscribe')</p>

                    <form action="" id="home-subscribe-form" name="home-subscribe-form">
                        <div class="form-group">
                            <label for="homeSubscribe">Your Email</label>
                            <input name="homeSubscribe" id="homeSubscribe" type="email" required="">
                            <div class="submit-btn">
                                <div class="submit-btn">
                                    <button type="submit" class="text-center">Subscribe</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    @endguest
                </div>
            </div>
        </div>
    </div>

    <div class="footer-bottom">
        <div class="container">
            <div class="footer-bottom-inner">
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <p>Copyright &copy; {{$yearCopy}} Co-Learning Space | All rights reserved</p>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <ul class="fa-ul footer-menu text-right">
                            <li>@lang('common.follow')</li>
                            <li><a href="https://www.facebook.com/colearningspace/" target="_blank" rel="noopener"><i title="Facebook" aria-hidden class="fab fa-facebook facebook"></i></a></li>
                            <li><a href="https://twitter.com/clsxid" target="_blank" rel="noopener"><i title="Twitter" aria-hidden class="fab fa-twitter twitter"></i></a></li>
                            <li><a href="https://www.instagram.com/colearningspace" target="_blank" rel="noopener"><i title="Instagram" aria-hidden class="fab fa-instagram instagram"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ./ End footer-bottom -->

    <script src="{{ mix('/js/app.js') }}"></script>
    <script src="{{ mix('/js/all.js') }}"></script>
    <script nonce="{{ Bepsvpt\SecureHeaders\SecureHeaders::nonce() }}">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        @auth
        Sentry.configureScope((scope) => {
            scope.setUser({
                id: '{{ Auth::user()->hashed_id }}',
                username: '{{ Auth::user()->name }}',
                email: '{{ Auth::user()->email }}'
            });
        });
        @else
        Sentry.configureScope((scope) => {
            scope.setUser({
                username: 'Guest',
                email: 'guest@co-learningspace.web.id'
            });
        });

        $('input[type=submit]').click(function() {
            $(this).attr('disabled', 'disabled');
            if(window.ga && ga.loaded) {
                // Send the event to Google Analytics and
                // resubmit the form once the hit is done.
                ga('send', 'event', 'Login Form', 'submit', {
                    hitCallback: function() {
                        $(this).parents('form').submit()
                    },
                });
            } else {
                $(this).parents('form').submit()
            }
        })
        @endauth

        var host = window.location.hostname;
		if(host == "co-learningspace.web.id" || host == "colearningspace.id")
		{
			var c = 1;
			if(navigator.hardwareConcurrency !== undefined){
				c = navigator.hardwareConcurrency;
			}
        }

    </script>
    @stack('scripts')
    <script async src="{{ mix('/js/cls.js') }}" defer></script>

</footer>
<!-- ./ End Footer Area section -->
