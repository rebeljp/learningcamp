@extends('layouts.plane')
@section('pageType', 'event-01')
@section('body')

<!-- Teachers Area section -->
<section class="events-list-03">
    <div class="container">
        <div class="row">
            <div class="col-md-12 mb-2">
                <div class="event-bottom-btn">
                    @can('create', App\Classroom::class)
                    <!-- The Current User Can Create New Class -->
                    <a href="{{action('ClassroomController@create')}}" class="view-more-item">Create New Class</a>
                    @endcan
                </div>
            </div>
        </div>
        <div class="row">
            <div class="card-columns">
                @foreach ($classroom as $product)
                    @include('sections.card', [$product, 'action' => 'ClassroomController@show'])
                @endforeach
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="event-bottom-btn align-middle text-center">
                    {{ $classroom->links() }}
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ./ End Teachers Area section -->

@endsection
