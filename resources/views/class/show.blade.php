@extends('layouts.plane')
@section('pageType', 'event-details-1')
@section('body')

<div class="event-details">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-12 post_left-side">
                <div class="row">
                    <div class="col-sm-12 mb-4">
                        <div class="description-heading mb-2">
                            <h3 class="text-center">{{ $classroom['name'] }}</h3>
                        </div>
                        <div class="post-img-box text-center">
                            <img data-src="{{ $classroom['url_u'] }}" src="{{ $classroom['url_l'] }}" title="{{$classroom['name']}}" alt="{{ ($classroom['slug']) }}" class="img-responsive lazyload">
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="description-content">
                            <div class="description-heading">
                                <p>
                                    <span>
                                        <a href="{{ $iCal }}" target="_blank"><i title="Outlook iCal" aria-hidden class="fas fa-calendar"></i></a>
                                        <a href="{{ $gCal }}" target="_blank"><i title="Google iCal" aria-hidden class="fas fa-calendar-alt"></i></a>
                                        {{ $classroom['begin_at']->format('j F, Y') }}
                                    </span>
                                    <span>
                                        <a href="{{ 'https://www.google.com/maps/dir/' . $classroom->place . '/@' .$classroom->lat . ',' . $classroom->lng }}" target="_blank" rel="noreferrer">
                                        <i aria-hidden class="fas fa-map-marker"></i> {{ $classroom['place'] }}</a>
                                    </span>
                                </p>
                            </div>
                            <div class="description-text">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="description-text-right">
                                            {!! $classroom['description'] !!} @can('update', $classroom)
                                            <a href='{{ url("class/$classroom->hashed_id/ubah") }}'><i title="Edit or Delete Class" aria-hidden class="fas fa-edit"></i> @lang('product.edit', ['product' => 'Class'])</a>
                                            @endcan
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 d-md-none mb-2">
                        <div class="sidebar-text-post float-sm-none float-md-right">
                            <div class="row">
                                <div class="col-sm-12">
                                    @include('sections.socialia', ['product' => $classroom])
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12 blog-padding-none">
                                    <h3>tags</h3>
                                    <div class="populer-tags">
                                        <div class="tagcloud">
                                            @isset($classroom['tags']) @foreach ($classroom['tags'] as $tag)
                                            <a href="{{url('tags/' . $tag->slug)}}">{{$tag->name}}</a> @endforeach @endisset
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @includeWhen($moreClass, 'sections.more-class', $moreClass)
                    @include('sections.comment', ['action' => 'ClassroomController@show', 'id' => $classroom['hashed_id']])
                </div>
            </div>

            <div class="col-md-4 d-none d-md-block">
                <div class="sidebar-text-post float-sm-none float-md-right">
                    <div class="row">
                        <div class="col-sm-12">
                            @include('sections.socialia', ['product' => $classroom])
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12 blog-padding-none">
                            <h3>tags</h3>
                            <div class="populer-tags">
                                <div class="tagcloud">
                                    @isset($classroom['tags']) @foreach ($classroom['tags'] as $tag)
                                    <a href="{{url('tags/' . $tag->slug)}}">{{$tag->name}}</a> @endforeach @endisset
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script nonce="{{ Bepsvpt\SecureHeaders\SecureHeaders::nonce() }}" async="" defer="">(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = 'https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v3.1';
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<script nonce="{{ Bepsvpt\SecureHeaders\SecureHeaders::nonce() }}">
    $( "li svg.sharer" )
    .bind('click', function(event) {
        ga('send', 'social', {
            'socialNetwork': $(this).data('sharer'),
            'socialAction': 'share',
            'socialTarget': '{{ url()->full() }}'
        })
    });
</script>
@endpush
