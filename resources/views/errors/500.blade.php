@extends('layouts.plane')
@section('pageType', 'contact')
@section('body')
<div class="content">
    <h3 class="title text-center">Sorry, we are having a temporary problem. We have been alerted and will be rolling out a fix soon.</h3>

    @if(app()->bound('sentry') && !empty(Sentry::getLastEventID()))
        <h5 class="subtitle text-center">Event ID: {{ Sentry::getLastEventID() }}</h5>
    @endif
</div>

@endsection
@push('scripts')
<script>
  Sentry.init({
    dsn: "{{ env('APP_ENV') }}",
    eventId: "{{ Sentry::getLastEventID() }}",
    beforeSend(event) {
      // Check if it is an exception, if so, show the report dialog
      if (event.exception) {
        Sentry.showReportDialog();
      }
      return event;
    }
  });
</script>
@endpush
