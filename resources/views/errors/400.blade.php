@extends('layouts.plane')
@section('pageType', 'contact')
@section('body')
<div class="content">
    <h3 class="title text-center">Page Not Found.</h3>

    @if(app()->bound('sentry') && !empty(Sentry::getLastEventID()))
        <h5 class="subtitle text-center">Error ID: {{ Sentry::getLastEventID() }}</h5>
    @endif
</div>

@endsection
@push('scripts')
<script>
  Sentry.init({
    dsn: "{{ env('APP_ENV') }}",
    eventId: "{{ Sentry::getLastEventID() }}",
    beforeSend(event) {
      // Check if it is an exception, if so, show the report dialog
      if (event.exception) {
        Sentry.showReportDialog();
      }
      return event;
    }
  });
</script>
@endpush
