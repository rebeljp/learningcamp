<div class="sidebar-text-post float-sm-none float-md-right">
    <div class="row">
        <div class="col-sm-12">
            @include('sections.socialia', ['product' => $classroom])
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 blog-padding-none">
            <h3>tags</h3>
            <div class="populer-tags">
                <div class="tagcloud">
                    @isset($classroom['tags']) @foreach ($classroom['tags'] as $tag)
                    <a href="{{url('tags/' . $tag->slug)}}">{{$tag->name}}</a> @endforeach @endisset
                </div>
            </div>
        </div>
    </div>
</div>
