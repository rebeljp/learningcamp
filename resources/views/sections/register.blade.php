<!-- Start Register Area section -->
<section class="register-area">
	<div class="overlay-bg">
		<div class="container">
			<div class="row">

				<div class="col-sm-5">
					<div class="row">
						<div class="form-full-box">
							<div class="header-box-top">
								<div class="col-sm-12 section-header-box">
									<div class="section-header">
										<h2>register now</h2>
									</div><!-- ends: .section-header -->
								</div>
								<p>Create your free account now and get immediate access to 100s of online courses.</p>
							</div>
	                        <form>
	                            <div class="register-form">
	                                <div class="row">
	                                    <div class="col-xs-12">
	                                        <div class="form-group">
	                                            <input class="form-control" name="name" placeholder="Name" required="" type="text">
	                                        </div>
	                                    </div>

	                                    <div class="col-xs-12">
	                                        <div class="form-group">
	                                            <input class="form-control" name="email" placeholder="Email" required="" type="email">
	                                        </div>
	                                    </div>
	                                    <div class="col-xs-12">
	                                        <div class="form-group">
	                                            <input class="form-control" name="phone" placeholder="Phone" required="" type="text">
	                                        </div>
	                                    </div>
	                                    <div class="col-xs-12 register-btn-box">
	                                        <button class="register-btn" type="submit">Submit Now</button>
	                                    </div>
	                                </div>
	                            </div>
	                        </form>
                        </div>
					</div>
				</div>

				<div class="col-sm-7 form-content">
					<div class="row">
						<div class="col-sm-12 section-header-box">
							<div class="section-header section-header-l">
								<h2>Are You Ready for Study?</h2>
							</div><!-- ends: .section-header -->
						</div>
					</div>
					<p></p>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- ./ End Register Area section -->
