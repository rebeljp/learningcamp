<div class="col-sm-12 col-md-12">
    <div class="more-events">
        <h3>@lang('product.upcoming', ['product' => 'Event'])</h3>
        <div class="card-columns">
            @foreach ($moreEvent as $product)
                @include('sections.card', [$product, 'action' => 'EventController@show'])
            @endforeach
        </div>
    </div>
</div><!--End .row-->
