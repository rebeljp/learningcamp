<!-- Start Blog page Area section -->
<section class="blog-area">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 section-header-box">
				<div class="section-header">
					<h2>Our Latest Blog</h2>
					<p></p>
				</div><!-- ends: .section-header -->
			</div>
		</div>

		<div class="row">
			<div class="col-sm-4 latest-news-single">
				<div class="blog-single-box">
					<div class="img-box">
						<img src="images/index-02/news-01.jpg" alt="" class="img-responsive">
					</div>
					<div class="blog-content">
						<h3><a href="#">the future of Web Design</a></h3>
						<p class="blog-time">
							<span>
								<i aria-hidden class="fas fa-calendar event-icon"></i>
								12 July, 2017
							</span>
							<span>
								<i aria-hidden class="fas fa-user"></i>
								John Doe
							</span>
							<span>
								<i aria-hidden class="fas fa-comment"></i>
								12
							</span>
						</p>
						<div class="content-bottom">
							<p></p>
							<ul class="fa-ul">
								<li class="first-item"><a href="#">Read More<i aria-hidden class="fas fa-long-arrow-right blog-btn-icon"></i></a></li>
								<li><a href=""><i title="Share on Facebook" aria-hidden class="fab fa-facebook facebook"></i></a></li>
								<li><a href=""><i title="Share on Twitter" aria-hidden class="fab fa-twitter twitter"></i></a></li>
								<li><a href=""><i title="Share on Youtube" aria-hidden class="fab fa-youtube youtube"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-4 latest-news-single">
				<div class="blog-single-box">
					<div class="img-box">
						<img src="images/index-02/news-02.jpg" alt="" class="img-responsive">
					</div>
					<div class="blog-content">
						<h3><a href="#">the future of Web Design</a></h3>
						<p class="blog-time">
							<span>
								<i aria-hidden class="fas fa-calendar event-icon"></i>
								12 July, 2017
							</span>
							<span>
								<i aria-hidden class="fas fa-user"></i>
								John Doe
							</span>
							<span>
								<i aria-hidden class="fas fa-comment"></i>
								12
							</span>
						</p>
						<div class="content-bottom">
							<p></p>
							<ul class="fa-ul">
								<li class="first-item"><a href="#">Read More<i aria-hidden class="fas fa-long-arrow-right blog-btn-icon"></i></a></li>
								<li><a href=""><i title="Share on Facebook" aria-hidden class="fab fa-facebook facebook"></i></a></li>
								<li><a href=""><i title="Share on Twitter" aria-hidden class="fab fa-twitter twitter"></i></a></li>
								<li><a href=""><i title="Share on Youtube" aria-hidden class="fab fa-youtube youtube"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-4 latest-news-single">
				<div class="blog-single-box">
					<div class="img-box">
						<img src="images/index-02/news-03.jpg" alt="" class="img-responsive">
					</div>
					<div class="blog-content">
						<h3><a href="#">the future of Web Design</a></h3>
						<p class="blog-time">
							<span>
								<i aria-hidden class="fas fa-calendar event-icon"></i>
								12 July, 2017
							</span>
							<span>
								<i aria-hidden class="fas fa-user"></i>
								John Doe
							</span>
							<span>
								<i aria-hidden class="fas fa-comment"></i>
								12
							</span>
						</p>
						<div class="content-bottom">
							<p></p>
							<ul class="fa-ul">
								<li class="first-item"><a href="#">Read More<i aria-hidden class="fas fa-long-arrow-right blog-btn-icon"></i></a></li>
								<li><a href=""><i title="Share on Facebook" aria-hidden class="fab fa-facebook facebook"></i></a></li>
								<li><a href=""><i title="Share on Twitter" aria-hidden class="fab fa-twitter twitter"></i></a></li>
								<li><a href=""><i title="Share on Youtube" aria-hidden class="fab fa-youtube youtube"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</section>
<!-- ./ End blog Area section -->
