<div class="card">
    <img data-src="{{ $product['url_u'] }}" src="{{ $product['url_l'] }}" alt="{{ $product['slug'] }}" class="card-img-top rounded lazyload" title="{{$product['name']}}">
    <div class="card-body">
        <h5 class="card-title">
            <a href="{{ action($action, ['id' => $product['hashed_id'], 'name' => $product['slug']]) }}" title="{{$product['name']}}">
                {{ $product['name'] }}
            </a>
        </h5>
    </div>
    <div class="card-footer">
        <p class="card-text">
            <a href="{{ 'https://www.google.com/maps/dir/' . $product->place . '/@' .$product->lat . ',' . $product->lng }}" target="_blank" rel="noreferrer">
            <i aria-hidden class="fas fa-map-marker event-icon"></i> {{ $product['place'] }}</a>
        </p>
        <p class="card-text">
            <i aria-hidden class="fas fa-calendar event-icon"></i>
            <small class="text-muted">
                {{ $product['begin_at']->format('D, j F Y H:i') . ' - ' . $product['end_at']->format('H:i')}}
            </small>
        </p>
    </div>
</div>
