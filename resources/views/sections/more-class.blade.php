<div class="col-sm-12 col-md-12">
    <div class="more-events">
        <h3>@lang('product.upcoming', ['product' => 'Class'])</h3>
        <div class="card-columns">
            @foreach ($moreClass as $product)
                @include('sections.card', [$product, 'action' => 'ClassroomController@show'])
            @endforeach
        </div>
    </div>
</div><!--End .row-->
