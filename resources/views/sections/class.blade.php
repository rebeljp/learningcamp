<!-- Start Up Comming Events Area Section -->
<section id="class-area" class="events-area">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 section-header-box">
				<div class="section-header">
					<h2>@lang('product.new', ['product' => 'Class'])</h2>
					<p></p>
				</div><!-- ends: .section-header -->
			</div>
		</div>

		<div class="card-columns">
            @foreach ($classes as $product)
                @include('sections.card', [$product, 'action' => 'ClassroomController@show'])
            @endforeach
		</div>

		<div class="row mt-4">
			<div class="col-md-12">
				<div class="event-bottom-btn">
					<a href="{{ url('/class') }}" class="view-more-item ">@lang('product.more', ['product' => 'classes'])</a>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- ./ End Events Area section -->
