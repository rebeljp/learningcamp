<div class="col-sm-12 order-sm-last">
    <div class="more-events">
        <h3>@lang('product.comment')</h3>
        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="pills-fb-tab" data-toggle="pill" href="#pills-fb" role="tab" aria-controls="pills-fb"
                    aria-selected="true">Facebook</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="pills-disq-tab" data-toggle="pill" href="#pills-disq" role="tab" aria-controls="pills-disq"
                    aria-selected="false">Disqus</a>
            </li>
        </ul>
        <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade show active" id="pills-fb" role="tabpanel" aria-labelledby="pills-fb-tab">
                <div id="fb-root"></div>
                <div class="fb-comments" data-href="{{ action($action, ['id' => $id]) }}" data-numposts="5"></div>
            </div>
            <div class="tab-pane fade" id="pills-disq" role="tabpanel" aria-labelledby="pills-disq-tab">
                <div id="disqus_thread"></div>
            </div>
        </div>
    </div>
</div>
