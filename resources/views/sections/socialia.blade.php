<div class="share-socila-link">
    <h3>SHARE</h3>
    <div class="social-icon">
        <div class="row">
            <div class="col-sm-12">
                <ul class="fa-ul">
                    <li class="fa-li">
                        <a data-sharer="facebook" data-url="{{url()->full()}}" class="fab fa-facebook facebook sharer fa-fw fa-2x"></a>
                    </li>
                    <li class="fa-li">
                        <a data-sharer="whatsapp" data-url="{{url()->full()}}" data-title="{{$product['name']}}" class="fab fa-whatsapp whatsapp sharer fa-fw fa-2x"></a>
                    </li>
                    <li class="fa-li">
                        <a data-sharer="twitter" data-url="{{url()->full()}}" data-title="{{$product['name']}}" data-hashtags="colearningspace,cls" class="fab fa-twitter twitter sharer fa-fw fa-2x"></a>
                    </li>
                    <li class="fa-li">
                        <a data-sharer="line" data-url="{{url()->full()}}" data-title="{{$product['name']}}" class="fab fa-line line sharer fa-fw fa-2x"></a>
                    </li>
                    <li class="fa-li">
                        <a data-sharer="linkedin" data-url="{{url()->full()}}" class="fab fa-linkedin linkedin sharer fa-fw fa-2x"></a>
                    </li>
                    <li class="fa-li">
                        <a data-sharer="googleplus" data-url="{{url()->full()}}" class="fab fa-google-plus google sharer fa-fw fa-2x"></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
