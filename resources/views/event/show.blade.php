@extends('layouts.plane')
@section('pageType', 'event-details-1')
@section('body')

<div class="event-details">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-12 post_left-side">
                <div class="row">
                    <div class="col-sm-12 mb-4">
                        <div class="description-heading mb-2">
                            <h3 class="text-center">{{ $event['name'] }}</h3>
                        </div>
                        <div class="post-img-box text-center">
                            <img data-src="{{ $event['url_u'] }}" src="{{ $event['url_l'] }}" title="{{$event['name']}}" alt="{{ $event['slug'] }}" class="img-responsive lazyload">
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="description-content">
                            <div class="description-heading">
                                <p>
                                    <span>
                                        <a href="{{ $iCal }}" target="_blank"><i title="Outlook iCal" aria-hidden class="fas fa-calendar"></i></a>
                                        <a href="{{ $gCal }}" target="_blank"><i title="Google iCal" aria-hidden class="fas fa-calendar-alt"></i></a>
                                        {{ $event['begin_at']->format('j F, Y') }}
                                    </span>
                                    <span>
                                        <a href="{{ 'https://www.google.com/maps/dir/' . $event->place . '/@' .$event->lat . ',' . $event->lng }}" target="_blank" rel="noreferrer">
                                        <i aria-hidden class="fas fa-map-marker"></i> {{ $event['place'] }}</a>
                                    </span>
                                </p>
                            </div>
                            <div class="description-text">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="description-text-right">
                                            {!! $event['description'] !!}
                                            @can('update', $event)
                                                <a href='{{ url("event/$event->hashed_id/ubah") }}'><i title="Edit or Delete Class" aria-hidden class="fas fa-edit"></i> @lang('product.edit', ['product' => 'Event'])</a>
                                            @endcan
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 d-md-none mb-2">
                        <div class="sidebar-text-post float-sm-none float-md-right">
                            <div class="row">
                                <div class="col-sm-12">
                                    @include('sections.socialia', ['product' => $event])
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12 blog-padding-none">
                                    <h3>tags</h3>
                                    <div class="populer-tags">
                                        <div class="tagcloud">
                                            @isset($event['tags']) @foreach ($event['tags'] as $tag)
                                            <a href="{{url('tags/' . $tag->slug)}}">{{$tag->name}}</a> @endforeach @endisset
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @includeWhen($moreEvent, 'sections.more-event', $moreEvent)
                    @include('sections.comment', ['action' => 'EventController@show', 'id' => $event['hashed_id']])
                </div>
            </div>

            <div class="col-md-4 d-none d-md-block">
                <div class="sidebar-text-post float-sm-none float-md-right">
                    <div class="row">
                        <div class="col-sm-12">
                            @include('sections.socialia', ['product' => $event])
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12 blog-padding-none">
                            <h3>tags</h3>
                            <div class="populer-tags">
                                <div class="tagcloud">
                                    @isset($event['tags']) @foreach ($event['tags'] as $tag)
                                        <a href="{{url('tags/' . $tag->slug)}}">{{$tag->name}}</a>
                                    @endforeach @endisset
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script nonce="{{ Bepsvpt\SecureHeaders\SecureHeaders::nonce() }}" async="" defer="">(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = 'https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v3.1';
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<script nonce="{{ Bepsvpt\SecureHeaders\SecureHeaders::nonce() }}">
    $( "li svg.sharer" )
    .bind('click', function(event) {
        ga('send', 'social', $(this).data('sharer'), 'share', "{{ url()->full() }}");
    });
</script>
@endpush

