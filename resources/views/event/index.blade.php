@extends('layouts.plane')
@section('pageType', 'event-01')
@section('body')

<!-- Teachers Area section -->
<section class="events-list-03">
    <div class="container">
        <div class="row">
            <div class="col-md-12 mb-2">
                <div class="event-bottom-btn">
                    @can('create', App\Event::class)
                    <!-- The Current User Can Create New Post -->
                    <a href="{{action('EventController@create')}}" class="view-more-item">Create New Event</a>
                    @endcan
                </div>
            </div>
        </div>
        <div class="row">
            <div class="card-columns">
                @foreach ($events as $product)
                    @include('sections.card', [$product, 'action' => 'EventController@show'])
                @endforeach
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="event-bottom-btn align-middle text-center">
                    {{ $events->links() }}
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ./ End Teachers Area section -->

@endsection
