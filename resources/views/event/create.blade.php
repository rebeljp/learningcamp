 @extends('layouts.plane')
 @section('pageType', 'event-details-1')
 @section('body')

<div class="event-details">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <form action="{{url('event')}}" id="form-submit" method="POST" class="eduread-register-form ">
                    @csrf
                    <p class="lead">Create New Event</p>
                    <div class="form-group">
                        <input class="required form-control" placeholder="Name *" type="text" name="name" id="name" value="{{ old('name') }}" required>
                        <input class="" type="hidden" name="public_id" id="public_id" value="{{ $publicId }}" required>
                    </div>
                    <div class="form-group">
                        {!! Form::select('category_id', $category, old('category_id'), ['placeholder' => "Select Category", 'class' => 'required form-control']) !!}
                    </div>
                    <div class="form-group">
                        <textarea class="required form-control" placeholder="Description *" name="description" id="description" rows="10" required>{{ old('description') }}</textarea>
                    </div>
                    <div class="form-group">
                        <input class="required form-control" placeholder="Find Location" type="text" name="location.search" id="location.search" value="{{ old('location.search') }}"
                            onFocus="geolocate()" required>
                    </div>
                    <div class="form-group">
                        <input class="required form-control" type="text" name="place" id="place" value="{{ old('place') }}"  readonly="">
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-6">
                                <input class="required form-control" placeholder="Latitude" type="text" name="lat" id="lat"
                                value="{{ old('lat') }}" required readonly="">
                            </div>
                            <div class="col-lg-6">
                                <input class="required form-control" placeholder="Longitude" type="text" name="lng" id="lng"
                                value="{{ old('lng') }}" required readonly="">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class='input-group date' id='datetimepicker1' data-target-input="nearest">
                                    <span class="input-group-prepend" data-target="#datetimepicker1" data-toggle="datetimepicker">
                                        <div class="input-group-text"><span class="fas fa-calendar-alt"></span></div>
                                    </span>
                                    <input type='text' class="form-control datetimepicker-input" data-target="#datetimepicker1" id="begin_at" name="begin_at" placeholder="Begin Date *" value="{{ old('begin_at') }}"/>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class='input-group date' id='datetimepicker2' data-target-input="nearest">
                                    <span class="input-group-prepend" data-target="#datetimepicker2" data-toggle="datetimepicker">
                                        <div class="input-group-text"><span class="fas fa-calendar-alt"></span></div>
                                    </span>
                                    <input type='text' class="form-control datetimepicker-input" data-target="#datetimepicker2" id="end_at" name="end_at" placeholder="End Date *" value="{{ old('end_at') }}" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <input class="required form-control" placeholder="Tag * tag1, tag2, tag3" type="text" name="tag" id="tag" value="{{ old('tag') }}" required>
                    </div>
                    <div class="form-group">
                        <div class='input-group date' id='datetimepicker3' data-target-input="nearest">
                            <span class="input-group-prepend" data-target="#datetimepicker3" data-toggle="datetimepicker">
                                <div class="input-group-text"><span class="fas fa-calendar-alt"></span></div>
                            </span>
                            <input type='text' class="form-control datetimepicker-input" data-target="#datetimepicker3" id="created_at" name="created_at" placeholder="Publish Date" value="{{ old('created_at') }}"/>
                        </div>
                    </div>
                    <div class="form-row align-items-center">
                        <div class="col-auto">
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="fab fa-facebook fa-fw text-facebook"></i></div>
                                </div>
                                @if ($facebook)
                                    {!! Form::select('postFacebook', $facebook, $selected, ['placeholder' => "Do not Post"]) !!}
                                @else
                                    <a href="{{ url('/login/facebook') }}" class="btn btn-facebook" target="_blank">Connect to Facebook</a>
                                @endif
                            </div>
                        </div>
                        <div class="col-auto">
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="fab fa-twitter fa-fw text-twitter"></i></div>
                                </div>
                                @if (0)
                                <select class="custom-select mr-sm-2" id="postTwitter">
                                    <option selected>Don't Post to Twitter</option>
                                    <option value="1">One</option>
                                    <option value="2">Two</option>
                                    <option value="3">Three</option>
                                </select>
                                @else
                                    <a href="{{ url('/login/twitter') }}" class="btn btn-twitter" target="_blank">Connect to Twitter</a>
                                @endif
                            </div>
                        </div>
                        <div class="col-auto">
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="fab fa-google-plus fa-fw text-google"></i></div>
                                </div>
                                @if (0)
                                <select class="custom-select mr-sm-2" id="postGoogle">
                                    <option selected>Don't Post to Google +</option>
                                    <option value="1">One</option>
                                    <option value="2">Two</option>
                                    <option value="3">Three</option>
                                </select>
                                @else
                                    <a href="{{ url('/login/gplus') }}" class="btn btn-google" target="_blank">Connect to Google Plus</a>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group register-btn">
                        <button id="upload_widget_opener" class="btn btn-cls">Upload and Crop images</button>
                    </div>
                    <div class="form-group register-btn">
                        <button type="button" id="btnSubmit" name="btnSubmit" class="btn btn-cls btn-lg btn-block">Buat Event</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@push('style')
	<link href="{{ mix('/css/editor.css') }}" rel="stylesheet preload" as="style" />
@endpush @push('scripts')
<script async src="{{ mix('/js/editor.js') }}"></script>
<script async defer src="https://widget.cloudinary.com/v2.0/global/all.js" type="text/javascript"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDJcPTuBWABrOJKMfPdhwcZVUHTI1AJObE&libraries=places&callback=initAutocomplete&language=id" type="text/javascript"></script>
<script nonce="{{ Bepsvpt\SecureHeaders\SecureHeaders::nonce() }}">
    var placeSearch;
    var autocomplete;
    var input = document.getElementById('location.search');
    var options = {
        componentRestrictions: {country: 'id'}
    };
    var host = window.location.hostname;
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
                center: geolocation,
                radius: position.coords.accuracy
            });
        });
    }

    function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(input,options);

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            // If getPlace() is not undefined (so if it exists), store the formatted_address (or whatever data is relevant to you) in the hidden input.
            if(autocomplete.getPlace() === undefined) {
                console.log('Undefined Place');
                return;
            }
            var place = autocomplete.getPlace();
            // Get the place details from the autocomplete object.
            $("input[name='place']").val(place.name);
            $("input[name='lat']").val(place.geometry.location.lat);
            $("input[name='lng']").val(place.geometry.location.lng);
        });
    }

    function geolocate(){
        var host = window.location.hostname;
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var geolocation = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                var circle = new google.maps.Circle({
                    center: geolocation,
                    radius: position.coords.accuracy
                });

                if(autocomplete !== undefined){
                    autocomplete.setBounds(circle.getBounds());
                }
            });
        }
    }

</script>
<script nonce="{{ Bepsvpt\SecureHeaders\SecureHeaders::nonce() }}">
    $(document).ready(function () {
        $('#datetimepicker1').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            sideBySide: true,
            showTodayButton: true,
            showClear: true,
            useCurrent: 'hour',
            locale: 'id'
        });

        $('#datetimepicker2').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            sideBySide: true,
            showTodayButton: true,
            showClear: true,
            useCurrent: 'hour',
            locale: 'id'
        });

        $('#datetimepicker3').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            sideBySide: true,
            showTodayButton: true,
            showClear: true,
            locale: 'id'
        });

        $("#datetimepicker1").on("change.datetimepicker", function (e) {
            $('#datetimepicker2').datetimepicker('minDate', e.date);
        });

        $("#datetimepicker2").on("change.datetimepicker", function (e) {
            $('#datetimepicker1').datetimepicker('maxDate', e.date);
        });

        var inscrybmde = new InscrybMDE({
            element: $("#description")[0],
            showIcons: ['heading','table'],
            promptURLs: true,
            autosave: {
                enabled: true,
                uniqueId: "{{ $publicId }}",
                delay: 5000,
            },
        });
        var form = document.getElementById("form-submit");

        $('#btnSubmit').click(function() {
            form.submit();
        });

        document.getElementById("upload_widget_opener").addEventListener("click", function() {
            cloudinary.openUploadWidget({
                cloudName: "cls-web-id",
                uploadPreset: "bosvjx0t_event",
                sources: ["local", "url"],
                showAdvancedOptions: true,
                cropping: true,
                croppingAspectRatio: 1,
                multiple: false,
                defaultSource: "local",
                publicId: '{{ $publicId }}',
                styles: {
                    palette: {
                        window: "#FFFFFF",
                        windowBorder: "#90A0B3",
                        tabIcon: "#0078FF",
                        menuIcons: "#5A616A",
                        textDark: "#000000",
                        textLight: "#FFFFFF",
                        link: "#0078FF",
                        action: "#FF620C",
                        inactiveTabIcon: "#0E2F5A",
                        error: "#F44235",
                        inProgress: "#0078FF",
                        complete: "#20B832",
                        sourceBg: "#E4EBF1"
                    },
                    fonts: {
                        default: null,
                        "'Fira Sans', sans-serif": {
                            url: "https://fonts.googleapis.com/css?family=Fira+Sans",
                            active: true
                        }
                    }
                }
            }, (err, info) => {
                if (!err) {
                    console.log("Upload Widget event - ", info);
                }
            });
        }, false);

    });
</script>
@endpush

