@extends('layouts.plane')
@section('pageType', 'event-details-1')
@section('body')

<div class="event-details">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <form action="/event/{{$event->hashed_id}}" id="form-submit" method="POST" class="eduread-register-form ">
                    @csrf
                    @method('PUT')
                    <p class="lead">Edit Event</p>
                    <div class="form-group">
                        <input type="hidden" name="id" id="id" value="{{ old('id', $event->id) }}" required>
                        <input class="required form-control" placeholder="Name *" type="text" name="name" id="name" value="{{ old('name', $event->name) }}" required>
                    </div>
                    <div class="form-group">
                        {!! Form::select('category_id', $category, old('category_id', $event->category_id), ['id' => 'category_id', 'class' => 'required form-control']); !!}
                    </div>
                    <div class="form-group">
                        <textarea class="required form-control" placeholder="Description *" name="description" id="description" rows="10" required>{{ old('description', $event->edit_description) }}</textarea>
                    </div>
                    <div class="form-group">
                        <input class="required form-control" placeholder="Find Location" type="text" name="location.search" id="location.search" value="{{ old('location.search') }}"
                            onFocus="geolocate()" required>
                    </div>
                    <div class="form-group">
                        <input class="required form-control" type="text" name="place" id="place" value="{{ old('place', $event->place) }}"  readonly="">
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-6">
                                <input class="required form-control" placeholder="Latitude" type="text" name="lat" id="lat"
                                value="{{ old('lat', $event->lat) }}" required readonly="">
                            </div>
                            <div class="col-lg-6">
                                <input class="required form-control" placeholder="Longitude" type="text" name="lng" id="lng"
                                value="{{ old('lng', $event->lng) }}" required readonly="">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class='input-group date' id='datetimepicker1'>
                                    <input type='text' class="form-control" id="begin_at" name="begin_at" placeholder="Begin Date *" value="{{ old('begin_at', $event->begin_at) }}"/>
                                    <span class="input-group-addon">
                                        <span class="fas fa-calendar-alt"></span>
                                    </span>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class='input-group date' id='datetimepicker2'>
                                    <input type='text' class="form-control" id="end_at" name="end_at" placeholder="End Date *" value="{{ old('end_at', $event->end_at) }}" />
                                    <span class="input-group-addon">
                                        <span class="fas fa-calendar-alt"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <input class="required form-control" placeholder="Tag * tag1, tag2, tag3" type="text" name="tag" id="tag" value="{{ old('tag', $tag) }}"
                            required>
                    </div>
                    <div class="form-group register-btn">
                        <button type="button" id="btnSubmit" name="btnSubmit" class="btn btn-primary btn-lg">Edit Event</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push('style')
	<link href="{{ mix('/css/editor.css') }}" rel="stylesheet preload" as="style" />
@endpush

@push('scripts')
    <script async src="{{ mix('/js/editor.js') }}"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDJcPTuBWABrOJKMfPdhwcZVUHTI1AJObE&libraries=places&callback=initAutocomplete&language=id" type="text/javascript" async defer></script>

	<script nonce="{{ Bepsvpt\SecureHeaders\SecureHeaders::nonce() }}">
        var placeSearch;
        var autocomplete;
        var input = document.getElementById('location.search');
        var options = {
            componentRestrictions: {country: 'id'}
        };
        var host = window.location.hostname;
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var geolocation = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                var circle = new google.maps.Circle({
                    center: geolocation,
                    radius: position.coords.accuracy
                });
            });
        }

        function initAutocomplete() {
            // Create the autocomplete object, restricting the search to geographical
            // location types.
            autocomplete = new google.maps.places.Autocomplete(input,options);

            // When the user selects an address from the dropdown, populate the address
            // fields in the form.
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                // If getPlace() is not undefined (so if it exists), store the formatted_address (or whatever data is relevant to you) in the hidden input.
                if(autocomplete.getPlace() === undefined) {
                    console.log('Undefined Place');
                    return;
                }
                var place = autocomplete.getPlace();
                // Get the place details from the autocomplete object.
                $("input[name='place']").val(place.name);
                $("input[name='lat']").val(place.geometry.location.lat);
                $("input[name='lng']").val(place.geometry.location.lng);
            });
        }

        function geolocate(){
            var host = window.location.hostname;
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    var geolocation = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };
                    var circle = new google.maps.Circle({
                        center: geolocation,
                        radius: position.coords.accuracy
                    });

                    if(autocomplete !== undefined){
                        autocomplete.setBounds(circle.getBounds());
                    }
                });
            }
        }
	</script>
	<script nonce="{{ Bepsvpt\SecureHeaders\SecureHeaders::nonce() }}">
        $(document).ready(function () {
            $('#datetimepicker1, #datetimepicker2').datetimepicker({
                format: 'YYYY-MM-DD HH:mm:ss',
                sideBySide: true,
                showTodayButton: true,
                showClear: true,
            });

            $('#tag').tagsinput({
                maxTags: 5,
                maxChars: 15,
                trimValue: true,
            });

            var inscrybmde = new InscrybMDE({
                element: $("#description")[0],
                showIcons: ['heading','table'],
                promptURLs: true,
                autosave: {
                    enabled: true,
                    uniqueId: "MyUniqueID",
                    delay: 5000,
                },
            });
            var form = document.getElementById("form-submit");

            $('#btnSubmit').click(function() {
                form.submit();
            });

        });
	</script>
@endpush
