@extends('layouts.pl_auth')
@section('title', 'CLS - Sign In')
@section('pageType', 'login')
@section('body')

<!-- Teachers Area section -->
<section class="login-area">
	<div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                {!! Form::open(['route' => 'login', 'method' => 'POST', 'class' => "eduread-register-form text-center", 'id' => "login-form", 'name' => "login-form"]) !!}
                    <div class="mx-auto pb-4">
                        <a href="/">
                            <img data-src="https://res.cloudinary.com/cls-web-id/image/upload/c_scale,w_200/v1524491338/images/CLS_Primary_Logo.png" src="https://res.cloudinary.com/cls-web-id/image/upload/e_blur:1000,c_scale,q_auto:low,w_200/v1524491338/images/CLS_Primary_Logo.png" alt="Co-LearningSpace.web.id" title="CLS" class="align-top lazyload">
                        </a>
                    </div>

					<p class="lead pb-2">@lang('auth.login.welcome')</p>
					<div class="form-group mb-3">
                        <input class="required form-control {{($errors->has('email') ? 'is-invalid' : '')}}" autocomplete="email" placeholder="@lang('auth.login.email')" type="email" name="email" id="email" value="{{ old('email') }}" required>
                        @if ($errors->has('email'))
                            <div class="invalid-feedback text-left" role="alert">{{ $errors->first('email') }}</div>
                        @endif
					</div>
					<div class="form-group mb-3">
                        {!! Form::password('password', ['autocomplete' => "current-password", 'class' => "required form-control " . ($errors->has('password') ? 'is-invalid' : ''), 'placeholder' => 'Password']) !!}
                        @if ($errors->has('email'))
                            <div class="invalid-feedback text-left" role="alert">{{ $errors->first('password') }}</div>
                        @endif
					</div>
                    <div class="form-group">
                        <div class="form-check">
                            <label>
                                <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}> @lang('auth.login.remember')
                            </label>
                        </div>
                    </div>
					<div class="form-group register-btn">
					 	<button type="submit" class="btn btn-primary btn-lg">Login</button>
					</div>

					<a href="{{ route('password.request') }}"><strong>@lang('auth.login.forgot')</strong></a>
					<p>@lang('auth.login.member') <a href="{{url('register')}}"><strong>@lang('auth.login.join')</strong></a></p>
                {!! Form::close() !!}
                <hr>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <a href="{{ url('/login/facebook') }}" class="btn btn-facebook"><i class="fab fa-facebook"></i> Facebook</a>
                    </div>
                </div>
			</div>
		</div>
	</div>
</section>
<!-- ./ End Teachers Area section -->
@endsection
@push('scripts')
<script nonce="{{ Bepsvpt\SecureHeaders\SecureHeaders::nonce() }}">
$('input[type=submit]').click(function() {
    $(this).attr('disabled', 'disabled');
    if(window.ga && ga.loaded) {
        // Send the event to Google Analytics and
        // resubmit the form once the hit is done.
        ga('send', 'event', 'Login Form', 'submit', {
            hitCallback: function() {
                $(this).parents('form').submit()
            },
        });
    } else {
        $(this).parents('form').submit()
    }
})
</script>
@endpush
