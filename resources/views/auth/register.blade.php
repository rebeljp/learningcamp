@extends('layouts.pl_auth')
@section('title', 'CLS - Sign Up')
@section('pageType', 'register')
@section('body')

<!-- Teachers Area section -->
<section class="register-area">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <form action="{{route('register')}}" method="POST" class="eduread-register-form text-center" id="signup-form" name="signup-form">
                    <div class="mx-auto pb-4">
                        <a href="/">
                            <img data-src="https://res.cloudinary.com/cls-web-id/image/upload/c_scale,w_200/v1524491338/images/CLS_Primary_Logo.png" src="https://res.cloudinary.com/cls-web-id/image/upload/e_blur:1000,c_scale,q_auto:low,w_200/v1524491338/images/CLS_Primary_Logo.png" alt="Co-LearningSpace.web.id" title="CLS" class="align-top lazyload">
                        </a>
                    </div>
                    @csrf
                    <p class="lead pb-2">@lang('auth.register.welcome')</p>
                    <div class="form-group mb-3">
                        <input class="required form-control" autocomplete="name" placeholder="@lang('auth.register.name')" type="text" name="name" id="name" value="{{ old('name') }}" required>
                        @if ($errors->has('name'))
                            <div class="invalid-feedback text-left" role="alert">{{ $errors->first('name') }}</div>
                        @endif
                    </div>
                    <div class="form-group mb-3">
                        <input class="required form-control" autocomplete="email" placeholder="@lang('auth.login.email')" type="email" name="email" id="email" value="{{ old('email') }}" required>
                        @if ($errors->has('email'))
                            <div class="invalid-feedback text-left" role="alert">{{ $errors->first('email') }}</div>
                        @endif
                    </div>
                    <div class="form-group mb-3">
                        <input class="required form-control" autocomplete="mobile" placeholder="@lang('auth.register.mobile')" type="tel" name="handphone" id="handphone" value="{{ old('handphone') }}" required>
                        @if ($errors->has('mobile'))
                            <div class="invalid-feedback text-left" role="alert">{{ $errors->first('mobile') }}</div>
                        @endif
                    </div>
                    <div class="form-group mb-3">
                        <input class="required form-control" autocomplete="organization" placeholder="@lang('auth.register.organization')" type="text" name="organization" id="organization" value="{{ old('organization') }}" required>
                        @if ($errors->has('organization'))
                            <div class="invalid-feedback text-left" role="alert">{{ $errors->first('organization') }}</div>
                        @endif
                    </div>
                    <div class="form-group mb-3">
                        <input class="required form-control" autocomplete="new-password" placeholder="Password" type="password" name="password" id="password" required>
                        @if ($errors->has('password'))
                            <div class="invalid-feedback text-left" role="alert">{{ $errors->first('password') }}</div>
                        @endif
                    </div>
                    <div class="form-group">
                        <input class="required form-control" placeholder="@lang('auth.register.confirm')" name="password_confirmation" id="password_confirmation"
                            type="password" required>
                    </div>
                    <div class="form-group register-btn">
                        <button type="submit" class="btn btn-primary btn-lg">@lang('auth.register.submit')</button>
                    </div>
                </form>
                <hr>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <a href="{{ url('/login/facebook') }}" class="btn btn-facebook"><i class="fab fa-facebook"></i> Facebook</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ./ End Teachers Area section -->
@endsection
@push('scripts')
<script nonce="{{ Bepsvpt\SecureHeaders\SecureHeaders::nonce() }}">
var handphone = document.querySelector("#handphone");
window.intlTelInput(handphone, {
    preferredCountries: "id",
    initialCountry: 'id',
    allowDropdown: false,
    nationalMode: false
});

$('input[type=submit]').click(function() {
    $(this).attr('disabled', 'disabled');
    if(window.ga && ga.loaded) {
        // Send the event to Google Analytics and
        // resubmit the form once the hit is done.
        ga('send', 'event', 'Login Form', 'submit', {
            hitCallback: function() {
                $(this).parents('form').submit()
            },
        });
    } else {
        $(this).parents('form').submit()
    }
})
</script>
@endpush
