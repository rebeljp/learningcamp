@extends('layouts.pl_auth')
@section('title', 'CLS - Forget Password')
@section('pageType', 'login')
@section('body')
<section class="login-area">
	<div class="container">
        <div class="row">
			<div class="col-sm-6 offset-3">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
				<form method="POST" action="{{ route('password.email') }}" class="eduread-register-form text-center">
					<p>@lang('passwords.lost')</p>
                    @csrf
					<div class="form-group row">
					  	<input autocomplete="username" class="required form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="E-Mail Address" name="email" id="email" type="text" value="{{ old('email') }}" required>

                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
					<div class="form-group register-btn">
					 	<button type="submit" class="btn btn-primary btn-lg">@lang('passwords.submit')</button>
					</div>
				</form>
			</div>
        </div>
	</div>
</section>

@endsection
