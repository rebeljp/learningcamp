@extends('layouts.plane')
@section('title', 'CLS - Verify E-Mail')
@section('pageType', 'register')
@section('body')

<!-- Teachers Area section -->
<section class="register-area">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">@lang('auth.verify.welcome')</div>

                    <div class="card-body">
                        @if (session('resent'))
                            <div class="alert alert-success" role="alert">
                                @lang('auth.verify.sent')
                            </div>
                        @endif

                        @lang('auth.verify.check')
                        @lang('auth.verify.failed'), <a name="resend" id="resend" href="{{ route('verification.resend') }}">@lang('auth.verify.retry')</a>.
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ./ End Teachers Area section -->
@endsection
<script nonce="{{ Bepsvpt\SecureHeaders\SecureHeaders::nonce() }}">
let $i = 1;
$('#resend').click(function() {
    if(window.ga && ga.loaded) {
        // Send the event to Google Analytics and
        // resubmit the form once the hit is done.
        ga('send', 'event', 'Verify Resend', 'submit', 'tries', $i);
        $i++;
    }
})
</script>

