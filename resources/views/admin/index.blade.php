@extends('layouts.plane')
@section('pageType', 'admin-panel-1')
@section('body')

<div class="event-details">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Simple Admin Page</h2>
                    <hr>
                    <br>
                    <ol class="fa-ul">
                        <li><a href="{{ url('admin/category') }}">
                            <span class="fa-li">
                                <i class="fas fa-book"></i>
                            </span>Manage Category</a>
                        </li>
                        <li><a href="{{ url('admin/category') }}">
                            <span class="fa-li">
                                <i class="fas fa-book"></i>
                            </span>Manage Class</a>
                        </li>
                        <li><a href="{{ url('user/admin/category') }}">
                            <span class="fa-li">
                                <i class="fas fa-book"></i>
                            </span>Manage Event</a>
                        </li>
                        <li><a href="{{ url('user/admin/category') }}">
                            <span class="fa-li">
                                <i class="fas fa-tag "></i>
                            </span>Manage Tag</a>
                        </li>
                        <li><a href="{{ url('user/admin/category') }}">
                            <span class="fa-li">
                                <i class="fas fa-user-check "></i>
                            </span>Manage User</a>
                        </li>
                        <li><a href="{{ url('user/admin/category') }}">
                            <span class="fa-li">
                                <i class="fas fa-user-graduate "></i>
                            </span>Manage Role</a>
                        </li>
                    </ol>
                        <hr>
                        <br>
                    <ol class="fa-ul">
                        <li><a href="{{ url('user/admin/category') }}">
                            <span class="fa-li">
                                <i class="fab fa-mailchimp "></i>
                            </span>Mail Blast</a>
                        </li>
                        <li><a href="{{ url('user/admin/category') }}">
                            <span class="fa-li">
                                <i class="fas fa-robot "></i>
                            </span>Manage Horizon Queue</a>
                        </li>
                        <li><a href="{{ url('user/admin/category') }}">
                            <span class="fa-li">
                                <i class="fas fa-file-invoice "></i>
                            </span>Manage Billing</a>
                        </li>
                    </ol>
                    <ol class="fa-ul">
                        <li><a href="{{ url('user/admin/category') }}">
                            <span class="fa-li">
                                <i class="fas fa-phone "></i>
                            </span>Edit Contact Us</a>
                        </li>
                        <li><a href="{{ url('user/admin/category') }}">
                            <span class="fa-li">
                                <i class="fas fa-people-carry "></i>
                            </span>Edit About Us</a>
                        </li>
                        <li><a href="{{ url('user/admin/category') }}">
                            <span class="fa-li">
                                <i class="fas fa-theater-masks "></i>
                            </span>Edit Privacy Policy</a>
                        </li>
                        <li><a href="{{ url('user/admin/category') }}">
                            <span class="fa-li">
                                <i class="fas fa-user-secret "></i>
                            </span>Edit Terms of Use</a>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
</div>
@endsection

@push('style')
	<link href="{{ mix('/css/editor.css') }}" rel="stylesheet preload" as="style" />
@endpush

@push('scripts')
    <script async src="{{ mix('/js/editor.js') }}"></script>
@endpush
