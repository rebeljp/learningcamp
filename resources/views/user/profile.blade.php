@extends('layouts.plane')
@section('pageType', 't-profile-01')
@section('body')

<!-- Teachers Area section -->
<section class="teacher-prolile-01">
    <div class="container">
        <div class="row">
            <div class="col-sm-3 t-profile-left">
                <div class="teacher-contact">
                    <img src="images/teachars/t-profile-01.jpg" alt="" class="img-responsive">
                    @auth
                    <h3>contact info</h3>
                    <p><span>Chat:</span></p>

                    <ul class="fa-ul">
                        <li><a href="">
                            <span class="fa-layers fa-2x fa-fw">
                                <i class="fa fa-circle"></i>
                                <i class="fab fa-facebook-f teacher-icon fa-inverse" data-fa-transform="shrink-3.5 down-1.6 right-1.25"></i>
                            </span>
                        </a></li>
                        <li><a href="">
                            <span class="fa-layers fa-2x fa-fw">
                                <i class="fa fa-circle"></i>
                                <i class="fab fa-twitter teacher-icon fa-inverse" data-fa-transform="shrink-6 down-.25 right-.25"></i>
                            </span>
                        </a></li>
                        <li><a href="">
                            <span class="fa-layers fa-2x fa-fw">
                                <i class="fa fa-circle"></i>
                                <i class="fab fa-google-plus teacher-icon fa-inverse"></i>
                            </span>
                        </a></li>
                        <li><a href="">
                            <span class="fa-layers fa-2x fa-fw">
                                <i class="fa fa-square"></i>
                                <i class="fab fa-linkedin teacher-icon fa-inverse"></i>
                            </span>
                        </a></li>
                    </ul>
                    @endauth
                </div>
            </div>
            <div class="col-sm-9 t-profile-right">
                <div class="row all-corsess-wapper">
                    <div class="col-sm-12">
                        <div class="all-courses">
                            <h3>{{ $user->name }}</h3>
                            <div class="profile__courses__inner">
                                <ul class="profile__courses__list fa-ul">
                                    <li><i class="fa fa-graduation-cap fa-fw"></i>Degree:</li>
                                    <li><i class="fa fa-star fa-fw"></i>Experience:</li>
                                    <li><i class="fa fa-heart fa-fw"></i>Hobbies:</li>
                                    <li><i class="fa fa-bookmark fa-fw"></i>My Courses:</li>
                                    <li><i class="fa fa-bookmark fa-fw"></i>Projects:</li>
                                </ul>
                                <ul class="profile__courses__right fa-ul">
                                    <li>PHD in Mathmatics</li>
                                    <li>20 Years in university Math</li>
                                    <li>Music, Dancing and Family</li>
                                    <li>Higher Math, Math Puzzle</li>
                                    <li>Map Creation</li>
                                </ul>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in finibus neque. Vivamus in ipsum quis elit vehicula tempus vitae quis lacus. Vestibulum interdum diam non mi cursus venenatis. Morbi lacinia libero et elementum vulputate. Vivamus et facilisis mauris. Maecenas nec massa auctor, ultricies massa eu, tristique erat. Vivamus in ipsum quis elit vehicula tempus vitae quis lacus. Eu pellentesque, accumsan tellus leo, ultrices mi dui lectus sem nulla eu.Eu pellentesque, accumsan tellus leo, ultrices mi dui </p>
                            <p>lectus sem nulla eu. Maecenas arcu, nec ridiculus quisque orci, vulputate mattis risus erat. lectus sem nulla eu.Eu pellentesque, accumsan tellus leo, ultrices mi dui lectus sem nulla eu. Maecenas arcu, nec ridiculus quisque orci, vulputate mattis risus erat.</p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="teacher_skill clearfix">
                        <div class="col-md-6 col-lg-6">
                            <h3>Classes</h3>
                            @foreach ($user['classrooms'] as $item)
                            <div class="row">
                                <div class="col-sm-12">
                                <p class="progress-bar-text">
                                    <a href="{{ action('ClassroomController@show', ['id' => $item['hashed_id'], 'name' => $item['slug']]) }}" title="{{$item['name']}}">
                                        {{$item->name}}
                                    </a>
                                </p>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <div class="col-md-6 col-lg-6">
                            <h3>Events</h3>
                            @foreach ($user['events'] as $item)
                            <div class="row">
                                <div class="col-sm-12">
                                    <p class="progress-bar-text">
                                        <a href="{{ action('EventController@show', ['id' => $item['hashed_id'], 'name' => $item['slug']]) }}" title="{{$item['name']}}">
                                        {{$item->name}}
                                        </a>
                                    </p>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ./ End Teachers Area section -->

@endsection
