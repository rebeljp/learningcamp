@extends('layouts.plane')
@section('pageType', 't-profile-01')
@section('body')

<div class="event-details">
    <div class="container">
        <div class="row mb-2 mt-2">
            <div class="col-md-12">
                <h2>User Settings</h2>
                <hr>
                <br>
                <div class="row">
                    <div class="col-4">
                        <div class="list-group" id="list-tab" role="tablist">
                            <a class="list-group-item list-group-item-action active" id="list-profile-list" data-toggle="list" href="#list-profile" role="tab"
                                aria-controls="profile">Profile</a>
                            <a class="list-group-item list-group-item-action" id="list-integration-list" data-toggle="list" href="#list-integration"
                                role="tab" aria-controls="integration">Integration</a>
                            <a class="list-group-item list-group-item-action" id="list-notification-list" data-toggle="list" href="#list-notification" role="tab"
                                aria-controls="notification">Notification</a>
                            <a class="list-group-item list-group-item-action" id="list-settings-list" data-toggle="list" href="#list-settings" role="tab"
                                aria-controls="settings">Settings</a>
                        </div>
                    </div>
                    <div class="col-8">
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="list-profile" role="tabpanel" aria-labelledby="list-profile-list">

                            </div>
                            <div class="tab-pane fade" id="list-integration" role="tabpanel" aria-labelledby="list-integration-list">
                                <div class="card">
                                    <div class="card-header">
                                        Integrations
                                    </div>
                                    <ul class="list-group list-group-flush">
                                        <li class="list-group-item">
                                            <i class="fab fa-facebook fa-fw fa-lg text-facebook"></i>
                                            @if (!empty($facebook))
                                                <a href="{{ url('/fb/logout') }}" class="btn btn-facebook" target="_blank">Disconnect {{$facebook['name']}}</a>
                                            @else
                                                <a href="{{ url('/login/facebook') }}" class="btn btn-facebook" target="_blank">Connect to Facebook</a>
                                            @endif
                                        </li>
                                        <li class="list-group-item">
                                            <i class="fab fa-twitter fa-fw fa-lg text-twitter"></i>
                                            @if (!empty($twitter))
                                                <a href="#" class="btn btn-twitter" target="_blank">Disconnect {{$twitter['name']}}</a>
                                            @else
                                                <a href="{{ url('/login/twitter') }}" class="btn btn-twitter" target="_blank">Connect to Twitter</a>
                                            @endif
                                        </li>
                                        <li class="list-group-item">
                                            <i class="fab fa-google-plus fa-fw fa-lg text-google"></i>
                                            @if (!empty($gplus))
                                                <a href="#" class="btn btn-google" target="_blank">Disconnect {{$gplus['name']}}</a>
                                            @else
                                                <a href="{{ url('/login/gplus') }}" class="btn btn-google" target="_blank">Connect to Google Plus</a>
                                            @endif
                                        </li>
                                    </ul>
                                </div>
                                <p class="card">
                                </p>
                            </div>
                            <div class="tab-pane fade" id="list-notification" role="tabpanel" aria-labelledby="list-notification-list">

                            </div>
                            <div class="tab-pane fade" id="list-settings" role="tabpanel" aria-labelledby="list-settings-list">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
 @push('style')
<link href="{{ mix('/css/editor.css') }}" rel="stylesheet preload" as="style" />
@endpush @push('scripts')
<script async src="{{ mix('/js/editor.js') }}"></script>
@endpush
