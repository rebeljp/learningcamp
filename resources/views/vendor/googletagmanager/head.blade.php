@if($enabled)
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-112445634-1"></script>
<script nonce="{{ Bepsvpt\SecureHeaders\SecureHeaders::nonce() }}">
    window.dataLayer = window.dataLayer || [];
    dataLayer = [{!! $dataLayer->toJson() !!}];
    @foreach($pushData as $item)
    dataLayer.push({!! $item->toJson() !!});
    function gtag(){dataLayer.push({!! $item->toJson() !!});}
    @endforeach
    if(typeof gtag === "function"){
        gtag('js', new Date());
        gtag('config', 'UA-112445634-1');
    }
</script>
<!-- Google Tag Manager -->
<script nonce="{{ Bepsvpt\SecureHeaders\SecureHeaders::nonce() }}">
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','{{ $id }}');
</script>
<!-- End Google Tag Manager -->
<!-- Google Analytics -->
<script nonce="{{ Bepsvpt\SecureHeaders\SecureHeaders::nonce() }}">
    window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
    ga('create', {
        trackingId: 'UA-112445634-1',
        cookieDomain: 'auto',
    });
    @auth
    $userId = '{{ Auth::user()->hashed_id }}';
    ga('set', 'userId', $userId);
    if (document.location.pathname.indexOf('user/' + $userId) > -1) {
        var page = document.location.pathname.replace('user/' + $userId, 'user');
        ga('set', 'page', page);
    }
    @endauth
    ga('set', 'forceSSL', true);
    ga('set', 'transport', 'beacon');
    ga('send', 'pageview');
</script>
<script async src='https://www.google-analytics.com/analytics.js'></script>
<!-- End Google Analytics -->
@endif
