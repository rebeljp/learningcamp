@if($enabled)
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id={{ $id }}"
height="0" width="0" class="invisible d-none"></iframe></noscript>
@endif
