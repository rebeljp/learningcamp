import * as Sentry from "@sentry/browser";

Sentry.init({
	dsn: process.env.MIX_SENTRY_DSN,
	release: process.env.MIX_APP_HASH,
	environment: process.env.MIX_NODE_ENV,
	integrations: [new Sentry.Integrations.Vue()]
});

window.Sentry = Sentry;
