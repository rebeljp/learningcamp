import lazySizes from "lazysizes";
import "lazysizes/plugins/respimg/ls.respimg";
import "lazysizes/plugins/optimumx/ls.optimumx";

// lazySizes.cfg == window.lazySizesConfig;
// Object.assign(lazySizes.cfg, {
// 	yourOptionA: "foo",
// 	yourOptionB: "bar",
// });

lazySizes.init();
