import { library, dom, config } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";
import { fab } from "@fortawesome/free-brands-svg-icons";

// If really necessary, entire styles can be loaded instead of specifying individual icons
library.add(fas);
library.add(fab);
// Make sure this is before any other `fontawesome` API calls
config.autoAddCss = false;
config.keepOriginalSource = false;
// Execute SVG replacement
dom.i2svg();
