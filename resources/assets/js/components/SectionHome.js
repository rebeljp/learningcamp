// import SectionWelcome from "./SectionWelcome.vue";
import SectionVideo from "./SectionVideo.vue";
// import SectionParents from "./SectionParents.vue";

// const sectionWelcome = new Vue({
// 	el: "#section-welcome",
// 	render: createElement => createElement(SectionWelcome) //< The important line
// });
const sectionVideo = new Vue({
	el: "#section-video",
	render: createElement => createElement(SectionVideo) //< The important line
});
// const sectionParents = new Vue({
// 	el: "#section-parents",
// 	render: createElement => createElement(SectionParents) //< The important line
// });
