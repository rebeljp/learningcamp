import SectionContact from "./SectionContact.vue";

const sectionContact = new Vue({
	el: "#section-contact",
	render: createElement => createElement(SectionContact) //< The important line
});
