var InfiniteScroll = require("infinite-scroll");
var imagesLoaded = require("imagesloaded");

// make imagesLoaded available for InfiniteScroll
InfiniteScroll.imagesLoaded = imagesLoaded;

if($(".pagination a.page-link.next").length > 0){
	let infScroll = new InfiniteScroll( ".card-columns", {
		// Infinite Scroll options...
		path: ".pagination a.page-link.next",
		append: ".card",
		status: ".page-load-status",
		hideNav: ".pagination"
	});
}
