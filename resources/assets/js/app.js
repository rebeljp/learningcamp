
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./sentry");
require("./bootstrap");
require("./fa");
require("./masonry");
require("./lazyload");

global.Vue = window.Vue = require("vue");

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.config.productionTip = false;

(function(){
	try{
		var msg = "          ╔═══╗  ╔╗     ╔═══╗\n          ║╔═╗║  ║║     ║╔═╗║\n          ║║ ╚╝  ║║     ║╚══╗\n          ║║ ╔╗  ║║ ╔╗  ╚══╗║\n          ║╚═╝║  ║╚═╝║  ║╚═╝║\n          ╚═══╝  ╚═══╝  ╚═══╝\n\n\n==========================================\nYou opened the console! Know some code,\ndo you? Want to work for one of the best\nstartups in Bandung? \nhttps://co-learningspace.web.id/contact-us\n==========================================";
		console.log(msg);
	}catch(e){}
})();
