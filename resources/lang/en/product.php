<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Product Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in product for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'new' => 'Latest :product',
    'more' => 'more :product',
    'comment' => 'Comment and Discussion',
    'upcoming' => 'Upcoming :product',
    'edit' => 'Edit :product'
];
