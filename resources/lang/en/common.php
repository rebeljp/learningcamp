<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Menu Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'contact' => 'Contact Us',
    'about' => 'About Us',
    'privacy' => 'Privacy Policy',
    'terms' => 'Terms of Use',
    'subscribe' => "Enter your email and we'll send you more information.",
    'follow' => 'Follow us :'
];
