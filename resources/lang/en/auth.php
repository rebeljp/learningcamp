<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'login' => [
        'welcome' => 'Welcome Back',
        'forgot' => 'Forgot Your Password?',
        'remember' => 'Remember Me',
        'email' => 'Email address',
        'join' => 'Join today',
        'member' => 'Not a member?',
    ],
    'register' => [
        'welcome' => 'Register New Account',
        'name' => 'Full Name',
        'mobile' => 'Mobile No',
        'organization' => 'Organization * Pribadi / Nama Kelompok',
        'confirm' => 'Confirm Password',
        'submit' => 'Register'
    ],
    'verify' => [
        'welcome' => 'Verify Your Email Address',
        'sent' => 'A fresh verification link has been sent to your email address.',
        'check' => 'Before proceeding, please check your email for a verification link.',
        'failed' => 'If you did not receive the email',
        'retry' => 'click here to request another'
    ]
];
