$(document).ready(function () {
	"use strict";

	//Mean Menu
	jQuery("header .main-menu").meanmenu({
		meanScreenWidth: "767"
	});

	$(window).on("load", function () {
		$("#status").fadeOut();
		$("body").delay(350).addClass("loaded");
	});

	// Contact Form
	$("#contactform").submit(function () {
		var action = $(this).attr("action");
		$("#message").slideUp(750, function () {
			$("#message").hide();
			$("#submit")
				.after("")
				.attr("disabled", "disabled");
			$.post(action, {
				name: $("#name").val(),
				email: $("#email").val(),
				subject: $("#subject").val(),
				comments: $("#comments").val()
			},
			function (data) {
				document.getElementById("message").innerHTML = data;
				$("#message").slideDown("slow");
				$("#contactform img.loader").fadeOut("slow", function () {
					$(this).remove();
				});
				$("#submit").removeAttr("disabled");
				if (data.match("success") !== null) $("#contactform").slideUp("slow");
			}
			);
		});
		return false;
	});

	//Video Popup
	$(".video-iframe").magnificPopup({
		type: "iframe",
		iframe: {
			markup: "<div class=\"mfp-iframe-scaler\">" +
				"<div class=\"mfp-close\"></div>" +
				"<iframe class=\"mfp-iframe\" frameborder=\"0\" allowfullscreen></iframe>" +
				"</div>",
			patterns: {
				youtube: {
					index: "youtube.com/",
					id: "v=",
					src: "https://www.youtube.com/embed/%id%?autoplay=1"
				}
			},
			srcAction: "iframe_src"
		}
	});

	//Popup
	$(".gallery-single-item").magnificPopup({
		delegate: "li .port-view",
		type: "image",
		gallery: {
			enabled: true
		},
		removalDelay: 300,
		mainClass: "mfp-fade"
	});

	// ************ Search On Click
	$(".search_btn").on("click", function (event) {
		event.preventDefault();
		$("#search").addClass("open");
		$("#search > form > input[type='search']").focus();
	});

	$("#search, #search button.close").on("click keyup", function (event) {
		if (event.target == this || event.target.className == "close" || event.keyCode == 27) {
			$(this).removeClass("open");
		}
	});

	// Slick slider	index version
	$(".slider-for").slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: true,
		asNavFor: ".slider-nav"
	});

	$(".slider-nav").slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		asNavFor: ".slider-for",
		dots: false,
		height: true,
		centerMode: true,
		centerPadding: "0px",
		focusOnSelect: true,
		variableWidth: false,
		arrows: true
	});

	if($("#parent-say-02").length > 0){
		// Parent Say's index-02
		$("#parent-say-02").owlCarousel({
			items: 3,
			lazyLoad: true,
			navigationText: ["<i class='fas fa-angle-left'></i>", "<i class='fas fa-angle-right'></i>"],
			slideSpeed: 500,
			paginationSpeed: 1000,
			rewindSpeed: 1000,
			navigation: true,
			pagination: false,
			responsive: {
				0: {
					items: 1,
					nav: false
				},
				480: {
					items: 1,
					nav: false
				},
				768: {
					items: 2,
					nav: true
				},
				992: {
					items: 2,
					nav: true,
					loop: false
				}
			}
		});
	}
});
