<?php

namespace App;

use Hashids;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Scout\Searchable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Searchable;
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'handphone', 'organization', 'facebook_id', 'google_id', 'twitter_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = ['hashed_id'];

    public function getHashedIdAttribute()
    {
        return Hashids::connection('user')->encode($this->attributes['id']);
    }

    public function level($roleName)
    {
        $auth = auth()->user()->load('role');
        return $auth->role['name'] === $roleName;
    }

    public function role()
    {
        return $this->belongsTo(\App\Role::class);
    }

    /**
     * Get the Location associated with the Event.
     */
    public function events()
    {
        return $this->hasMany(\App\Event::class);
    }

    /**
     * Get the Location associated with the Event.
     */
    public function classrooms()
    {
        return $this->hasMany(\App\Classroom::class);
    }
}
