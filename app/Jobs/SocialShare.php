<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Traits\Socialia;

class SocialShare implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, Socialia;

    private $api, $url, $pageId, $timestamp;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($api, $url, $pageId, $timestamp)
    {
        $this->api = $api;
        $this->url = $url;
        $this->pageId = $pageId;
        $this->timestamp = $timestamp;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->publishToPage($this->api, $this->url, $this->pageId, $this->timestamp);
    }
}
