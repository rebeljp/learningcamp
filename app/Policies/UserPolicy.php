<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    public function before($user)
    {
        if ($user->level('Super Admin')) {
            return true;
        }
    }

    /**
     * Determine whether the user CAN ACCESS ADMIN PANEL.
     *
     * @param \App\User $user
     * @param \App\User $model
     *
     * @return mixed
     */
    public function admin(User $user)
    {
        return $user->level('Admin');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param \App\User $user
     * @param \App\User $model
     *
     * @return mixed
     */
    public function update(User $user, User $model)
    {
        return $user->level('Admin') ||
        $model->id == $user->id;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param \App\User $user
     * @param \App\User $model
     *
     * @return mixed
     */
    public function delete(User $user, User $model)
    {
        return $user->level('Admin');
    }
}
