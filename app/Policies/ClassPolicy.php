<?php

namespace App\Policies;

use App\Classroom;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ClassPolicy
{
    use HandlesAuthorization;

    public function before($user)
    {
        if ($user->level('Super Admin')) {
            return true;
        }
    }

    /**
     * Determine whether the user can create classrooms.
     *
     * @param \App\User $user
     *
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->level('Admin') ||
        $user->level('Trainer');
    }

    /**
     * Determine whether the user can update the classroom.
     *
     * @param \App\User      $user
     * @param \App\Classroom $classroom
     *
     * @return mixed
     */
    public function update(User $user, Classroom $classroom)
    {
        return $user->level('Admin') ||
        $classroom->user_id == $user->id;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param \App\User $user
     * @param \App\Classroom $classroom
     *
     * @return mixed
     */
    public function delete(User $user, Classroom $classroom)
    {
        return $user->level('Admin') ||
        $classroom->user_id == $user->id;
    }
}
