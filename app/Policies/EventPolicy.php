<?php

namespace App\Policies;

use App\Event;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class EventPolicy
{
    use HandlesAuthorization;

    public function before($user)
    {
        if ($user->level('Super Admin')) {
            return true;
        }
    }

    /**
     * Determine whether the user can create events.
     *
     * @param \App\User $user
     *
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->level('Admin');
    }

    /**
     * Determine whether the user can update the event.
     *
     * @param \App\User  $user
     * @param \App\Event $event
     *
     * @return mixed
     */
    public function update(User $user, Event $event)
    {
        return $user->level('Admin') ||
        $event->user_id == $user->id;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param \App\User $user
     * @param \App\Event $event
     *
     * @return mixed
     */
    public function delete(User $user, Event $event)
    {
        return $user->level('Admin') ||
        $event->user_id == $user->id;
    }
}
