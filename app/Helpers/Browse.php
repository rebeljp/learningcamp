<?php

namespace App\Helpers;

use Spatie\GoogleTagManager\GoogleTagManager;

if (! GoogleTagManager::hasMacro('browse')) {
    GoogleTagManager::macro('browse', function ($order, array $products) {
        GoogleTagManager::set('event', 'list_view_item');
        $i = 1;
        foreach ($products as $product) {
            GoogleTagManager::set('ecommerce.'.$order.'.products', [
                [
                    'id' => $product['hashed_id'],
                    'name' => $product['name'],
                    'list_position' => $i,
                    'price' => 1.0,
                    'brand' => $product['user']['name'],
                    'geo' => $product['place'],
                    'category' => $product['category']['name'],
                ],
            ]);
            ++$i;
        }
    });
}
