<?php

namespace App;

use Carbon\Carbon;
use Cloudder;
use GrahamCampbell\Markdown\Facades\Markdown;
use Hashids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Scout\Searchable;
use Storage;

class Classroom extends Model
{
    use Searchable;
    use SoftDeletes;

    protected $guarded = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'begin_at',
        'end_at',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $appends = ['hashed_id', 'url_u', 'url_l', 'description', 'edit_description', 'slug' ];

    public function getHashedIdAttribute()
    {
        return Hashids::connection('classroom')->encode($this->attributes['id']);
    }

    public function getUrlUAttribute()
    {
        return Cloudder::secureShow('class/'.self::getHashedIdAttribute(), [
            'format' => 'jpg',
            'width' => '450',
            'height' => '450',
            'crop' => 'scale',
            'effect' => null,
        ]);
    }

    public function getUrlDAttribute()
    {
        return self::getUrlUAttribute();
    }

    public function getUrlLAttribute()
    {
        return Cloudder::secureShow('class/'.self::getHashedIdAttribute(), [
            'format' => 'jpg',
            'width' => '300',
            'height' => '300',
            'crop' => 'scale',
            'effect' => 'blur:1000',
            'quality' => 'auto:low'
        ]);
    }

    public function getDescriptionAttribute()
    {
        $hashIds = self::getHashedIdAttribute();
        $filePath = 'class/'.$hashIds.'.md';
        if (Storage::exists($filePath)) {
            return Markdown::convertToHtml(Storage::get($filePath));
        }
        return '<p> Unable to Find Description Data </p>';
    }

    public function getEditDescriptionAttribute()
    {
        $hashIds = self::getHashedIdAttribute();
        $filePath = 'class/'.$hashIds.'.md';
        if (Storage::exists($filePath)) {
            return Storage::get($filePath);
        }
        return 'Unable to Find Description Data';
    }

    public function getSlugAttribute()
    {
        return str_slug(strtolower($this->attributes['name']));
    }

    public function shouldBeSearchable()
    {
        $created = Carbon::parse($this->attributes['created_at']);
        return $created->lessThanOrEqualTo(now());
    }

    /**
     * Get the Category associated with the Event.
     */
    public function category()
    {
        return $this->belongsTo(\App\Category::class);
    }

    /**
     * Get the Location associated with the Event.
     */
    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }

    /**
     * Get all of the tags for the post.
     */
    public function tags()
    {
        return $this->morphToMany(\App\Tag::class, 'taggable');
    }

    public function scopePublished($query) {
        return $query->whereDate('created_at', '<=', now());
    }

    public function scopeDelayed($query) {
        return $query->whereDate('created_at', '>', now());
    }

    public function scopeFinish($query) {
        return $query->whereDate('end_at', '<=', now());
    }

    public function scopeRunning($query) {
        return $query->whereDate('begin_at', '<=', now())
            ->whereDate('end_at', '>', now());
    }

    public function scopeComing($query) {
        return $query->whereDate('begin_at', '=>', now());
    }
}
