<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tag extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    protected $appends = [ 'slug' ];

    public function getSlugAttribute()
    {
        return str_slug(strtolower($this->attributes['name']));
    }

    /**
     * Get all of the events that are assigned this tag.
     */
    public function events()
    {
        return $this->morphedByMany(\App\Event::class, 'taggable');
    }

    /**
     * Get all of the events that are assigned this tag.
     */
    public function classrooms()
    {
        return $this->morphedByMany(\App\Classroom::class, 'taggable');
    }
}
