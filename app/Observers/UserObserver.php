<?php

namespace App\Observers;

use App\User;
use Log;

class UserObserver
{
    /**
     * Listen to the User created event.
     *
     * @param \App\User $user
     */
    public function created(User $user)
    {
        Log::info('User Created' . $user->name);
    }

    /**
     * Listen to the User deleting event.
     *
     * @param \App\User $user
     */
    public function deleting(User $user)
    {
        Log::info('User Banned' . $user->name);
    }
}
