<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tag;
use App\Classroom;
use App\Event;
use Cache;
use GoogleTagManager;
use Carbon\Carbon;
use SEO;

class TagController extends Controller
{

    public function __construct()
    {
        $this->middleware('can:create,App\Tag', ['only' => ['create', 'store']]);
        $this->middleware('can:update,App\Tag', ['only' => ['edit', 'update']]);
        $this->middleware('can:delete,App\Tag', ['only' => ['delete']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param int $tag
     *
     * @return \Illuminate\Http\Response
     */
    public function show($tag)
    {
        GoogleTagManager::set('pageType', ['type' => 'product', 'name' => 'tags', 'view' => 'list product']);
        GoogleTagManager::set('event', 'view_item_list');

        $ttl = now()->addMonth();

        $tags = Tag::where('name', $tag)->select(['id', 'name', 'slug'])->first();

        $data['tags'] = Cache::remember('tag_' . $tag, $ttl, function () use ($tags, $ttl) {
            $classroom = Cache::remember('class_'. $tags->slug, $ttl, function () use ($tags) {
                $classroom = Classroom::whereHas('tags', function ($q) use($tags) {
                    $q->where('tags.id', $tags->id);
                })->with([
                    'user',
                    'category',
                ])->orderBy('begin_at', 'desc')->take(12)->get();

                foreach ($classroom as &$class) {
                    $class = array_add($class, 'action', 'ClassroomController@show');
                }
                return $classroom;
            });

            $events = Cache::remember('event_'. $tags->slug, $ttl, function () use ($tags) {
                $events = Event::whereHas('tags', function ($q) use($tags) {
                    $q->where('tags.id', $tags->id);
                })->with([
                    'user',
                    'category',
                ])->orderBy('begin_at', 'desc')->take(12)->get();

                foreach ($events as &$event) {
                    $event = array_add($event, 'action', 'EventController@show');
                }
                return $events;
            });

            if(empty($classroom)){
                return $events;
            }
            if(empty($events)){
                return $classroom;
            }

            return $classroom->sortKeys()->merge($events->sortKeys())->sortByDesc('begin_at');
        });

        GoogleTagManager::browse($tag, $data['tags']->toArray());

        $title = 'Tag - ' . $tag;
        $description = $tag;

        SEO::setTitle($title);
        SEO::setDescription($description);

        return view('tag.show')
            ->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $tag
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($tag)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $tag
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $tag)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $tag
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($tag)
    {
    }
}
