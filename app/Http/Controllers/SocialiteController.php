<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Socialite;
use App\User;

class SocialiteController extends Controller
{
    /**
     * Redirect the user to the Facebook authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToFacebookProvider()
    {
        return Socialite::driver('facebook')->scopes([
            "manage_pages",
            "publish_pages"
        ])->redirect();
    }

    /**
     * Obtain the user information from Facebook.
     *
     * @return void
     */
    public function handleProviderFacebookCallback()
    {
        $auth_user = Socialite::driver('facebook')->user();
        if (Auth::check()) {
            //Logged In
            User::where('id', Auth::id())
                ->update(['facebook_id' => $auth_user->token]);
            return redirect()->to('/'); // Redirect to a secure page
        }

        $user = User::updateOrCreate([
            'email' => $auth_user->email
        ], [
            'name' => $auth_user->name,
            'facebook_id' => $auth_user->token,
        ]);

        Auth::login($user, true);
        return redirect()->to('/'); // Redirect to a secure page
    }

    public function deAuthorize(Request $request){
        dd($request);
    }
}
