<?php

namespace App\Http\Controllers;

use App\Category;
use App\Classroom;
use App\Tag;
use App\Traits\Socialia;
use Cache;
use Carbon\Carbon;
use Cloudder;
use GoogleTagManager;
use Hashids;
use Illuminate\Http\Request;
use SEO;
use Spatie\CalendarLinks\Link;
use Storage;

class ClassroomController extends Controller
{
    use Socialia;
    private $api;

    public function __construct(\Facebook\Facebook $fBook)
    {
        $this->middleware('can:create,App\Classroom', ['only' => ['create']]);
        $this->middleware(function ($request, $next) use ($fBook) {
            if (!empty(auth()->user()->facebook_id)) {
                $fBook->setDefaultAccessToken(auth()->user()->facebook_id);
                $this->api = $fBook;
            }
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        GoogleTagManager::set('pageType', ['type' => 'product', 'name' => 'class', 'view' => 'list product']);
        GoogleTagManager::set('event', 'view_item_list');

        // from the request
        $page = request()->has('page') ? request()->get('page') : 1;
        // from user's preferences
        $perPage = 12;
        $ttl = now()->addMonth();

        $data['classroom'] = Cache::remember('class_p_'.$page, $ttl, function () use ($page, $perPage) {
            return Classroom::latest('end_at')
            ->with([
                'user',
                'category',
            ])
            ->simplePaginate($perPage, ['*'], 'page', $page);
        });

        GoogleTagManager::browse('classroom', $data['classroom']->toArray()['data']);

        $title = 'Class';
        $description = 'Pilih Kelas dan Gabung! atau Buat Kelas Baru';

        SEO::setTitle($title);
        SEO::setDescription($description);

        return view('class.index')
            ->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        GoogleTagManager::set('pageType', ['type' => 'product', 'name' => 'class', 'view' => 'create class']);
        SEO::setTitle('New Class');
        SEO::setDescription('');
        $data['publicId'] = Hashids::connection('classroom')->encode(now()->timestamp);

        $data['category'] = collect(Category::orderBy('name', 'ASC')->pluck('name', 'id'));
        $data['facebook'] = null;

        if (!empty(auth()->user()->facebook_id)) {
            $data = array_merge($data, $this->getFacebookPage($this->api));
        }
        return view('class.create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(\App\Http\Requests\StoreClass $request)
    {
        $data = $request->validated();
        $temps = explode(',', $data['tag']);

        foreach ($temps as &$temp) {
            $temp = trim($temp);
            Tag::firstOrCreate([
                'name' => $temp,
            ]);
        }

        $tags = Tag::whereIn('name', $temps)
            ->pluck('id')
            ->toArray();

        $last = Classroom::insertGetId([
            'name' => $data['name'],
            'category_id' => $data['category_id'],
            'place' => $data['place'],
            'lat' => $data['lat'],
            'lng' => $data['lng'],
            'user_id' => $request->user()['id'],
            'begin_at' => Carbon::parse($data['begin_at'])->toDateTimeString(),
            'end_at' => Carbon::parse($data['end_at'])->toDateTimeString(),
            'created_at' => Carbon::parse($data['created_at'])->toDateTimeString(),
        ]);

        $class = Classroom::findOrFail($last);
        $class->tags()->attach($tags);

        if (!Cloudder::rename('class/'.$data['public_id'], 'class/'.$class['hashed_id'])) {
            \Illuminate\Support\Facades\Log::error('Cannot Rename Image From ' . 'class/'.$data['public_id'] . ' TO ' . 'class/' . $class['hashed_id']);
        }

        Storage::put('class/'.$class['hashed_id'].'.md', $data['description']);

        for ($i=0; $i < 5; $i++) {
            Cache::forget('class_p_'.$i);
        }
        Cache::forget('class6');
        Cache::forget('moreClass3');
        Cache::forget('upcoming_2classes');
        foreach ($temps as $tag) {
            $slug = str_slug(strtolower($tag));

            if(Cache::has('tag_'. $slug)){
                Cache::forget('tag_'. $slug);
                Cache::forget('event_'. $slug);
            }
        }

        if(!empty($data['postFacebook'])){
            \App\Jobs\SocialShare::dispatch($this->api,
            action('ClassroomController@show', ['id' => $class['hashed_id'], 'name' => $class['slug']]),
            $data['postFacebook'],
            Carbon::parse($data['created_at'])->timestamp);

        }
        return redirect('class');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Classroom $classroom
     *
     * @return \Illuminate\Http\Response
     */
    public function show($classroom)
    {
        $data['classroom'] = Classroom::findOrFail(Hashids::connection('classroom')->decode($classroom)[0]);
        $tags = [];
        foreach ($data['classroom']['tags'] as $tag) {
            array_push($tags, $tag['name']);
        }

        $link = Link::create($data['classroom']['name'], $data['classroom']['begin_at'], $data['classroom']['end_at'])
            ->description(str_limit($data['classroom']['description'], 50))
            ->address($data['classroom']['place']);

        $data['iCal'] = $link->ics();
        $data['gCal'] = $link->google();
        $data['moreClass'] = Cache::get('moreClass3', function () use ($data) {
            $ttl = now()->addMonth();
            $cached = Classroom::latest('end_at')
            ->limit(3)
            ->whereNotIn('id', [$data['classroom']['id']])
            ->whereDate('begin_at', '>=', now())
            ->where(function ($query) {
                $query->whereDate('begin_at', '>=', now())
                        ->orwhereDate('end_at', '<=', now());
            })
            ->get();

            Cache::add('moreClass3', $cached, $ttl);

            return $cached;
        });

        GoogleTagManager::set('pageType', [
            'type' => 'productDetail',
            'name' => 'class',
            'page_id' => $classroom,
            'page_name' => $data['classroom']['name']
        ]);

        GoogleTagManager::set('event', 'view_item');

        GoogleTagManager::set('ecommerce.class.product.details', [
            [
                'name' => $data['classroom']->name,
                'id' => $data['classroom']->hashed_id,
                'price' => 1,
                'brand' => $data['classroom']['user']->name,
                'geo' => $data['classroom']['place'],
                'category' => $data['classroom']['category']->name,
            ],
        ]);

        SEO::setTitle($data['classroom']['name']);
        SEO::setDescription(str_limit($data['classroom']['description'], 50));
        SEO::metatags()->addMeta('article:published_time', $data['classroom']['created_at'], 'property');
        SEO::opengraph()->addProperty('type', 'articles');
        SEO::opengraph()->setArticle([
            'published_time' => $data['classroom']['created_at'],
            'modified_time' => $data['classroom']['created_at'],
            'expiration_time' => $data['classroom']['end_at'],
            'author' => $data['classroom']['user']['name'],
            'section' => 'class',
            'tag' => $tags,
        ]);
        SEO::setCanonical(url("/class/$classroom"));
        SEO::addImages($data['classroom']['url_u']);
        return view('class.show')
            ->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Classroom $classroom
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($classroom)
    {
        $data['classroom'] = Classroom::findOrFail(Hashids::connection('classroom')->decode($classroom)[0]);
        $this->authorize('update', $data['classroom']);
        $tags = [];
        foreach ($data['classroom']['tags'] as $tag) {
            array_push($tags, $tag['name']);
        }

        $data['tag'] = implode(',', $tags);

        GoogleTagManager::set('pageType', ['type' => 'product', 'name' => 'class', 'view' => 'edit class']);
        SEO::setTitle('Edit Class');
        $data['category'] = Category::orderBy('name', 'ASC')->pluck('name', 'id')->toArray();

        return view('class.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Classroom           $classroom
     *
     * @return \Illuminate\Http\Response
     */
    public function update(\App\Http\Requests\UpdateClass $request, $classroom)
    {
        // Retrieve the validated input data...
        $data = $request->validated();
        $temps = explode(',', $data['tag']);

        foreach ($temps as &$temp) {
            $temp = trim($temp);
            Tag::firstOrCreate([
                'name' => $temp,
            ]);
        }

        $tags = Tag::whereIn('name', $temps)
            ->pluck('id')
            ->toArray();

        $last = Classroom::where('id', $data['id'])
        ->update([
            'name' => $data['name'],
            'category_id' => $data['category_id'],
            'place' => $data['place'],
            'lat' => $data['lat'],
            'lng' => $data['lng'],
            'user_id' => $request->user()['id'],
            'begin_at' => Carbon::parse($data['begin_at'])->toDateTimeString(),
            'end_at' => Carbon::parse($data['end_at'])->toDateTimeString(),
        ]);

        $classroom = Classroom::findOrFail($last);
        $classroom->tags()->detach();
        $classroom->tags()->attach($tags);

        $publicId = Hashids::connection('classroom')->encode($classroom->id);

        Storage::put('class/'.$publicId.'.md', $data['description']);

        Cache::forget('class6');
        Cache::forget('moreClass3');

        return redirect('class');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Classroom $classroom
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($classroom)
    {
        $data['classroom'] = Classroom::findOrFail(Hashids::connection('classroom')->decode($classroom)[0]);
        $this->authorize('delete', $data['classroom']);
        Classroom::where('id' , $data['classroom']->id)->delete();
    }
}
