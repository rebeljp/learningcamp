<?php

namespace App\Http\Controllers;

use App\User;
use Hashids;

class ProfileInvoker extends Controller
{
    public function __invoke($user_hash)
    {
        return view('user.profile', [
            'user' => User::where('id', Hashids::connection('user')->decode($user_hash))->firstOrFail(),
        ]);
    }
}
