<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Hashids;
use Facebook\Facebook;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    private $api;

    public function __construct(Facebook $fBook)
    {
        $this->middleware(function ($request, $next) use ($fBook) {
            if (!empty(Auth::user()->facebook_id)) {
                $fBook->setDefaultAccessToken(Auth::user()->facebook_id);
                $this->api = $fBook;
            }
            return $next($request);
        });
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($user)
    {
        $data['user'] = User::findOrFail(Hashids::connection('user')->decode($user)[0]);
        $this->authorize('update', $data['user']);

        if (!empty(Auth::user()->facebook_id)) {
            try {
                $params = "name";
                $data['facebook'] = $this->api->get('/me?fields='.$params)->getGraphUser();
            } catch (FacebookSDKException $e) {
            }
        }

        return view('user.edit')->with($data);
    }
}
