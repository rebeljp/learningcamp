<?php

namespace App\Http\Controllers;

use App\Category;
use App\Event;
use App\Tag;
use App\Traits\Socialia;
use Cache;
use Carbon\Carbon;
use Cloudder;
use GoogleTagManager;
use Hashids;
use Illuminate\Http\Request;
use SEO;
use Spatie\CalendarLinks\Link;
use Storage;

class EventController extends Controller
{
    use Socialia;
    private $api;

    public function __construct(\Facebook\Facebook $fBook)
    {
        $this->middleware('can:create,App\Event', ['only' => ['create']]);
        $this->middleware(function ($request, $next) use ($fBook) {
            if (!empty(auth()->user()->facebook_id)) {
                $fBook->setDefaultAccessToken(auth()->user()->facebook_id);
                $this->api = $fBook;
            }
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        GoogleTagManager::set('pageType', ['type' => 'product', 'name' => 'event', 'view' => 'list event']);
        GoogleTagManager::set('event', 'view_item_list');

        // from the request
        $page = request()->has('page') ? request()->get('page') : 1;
        // from user's preferences
        $perPage = 12;
        $ttl = now()->addMonth();

        $data['events'] = Cache::remember('events_p_'.$page, $ttl, function () use ($page, $perPage) {
            return Event::latest('end_at')
            ->with([
                'user',
                'category',
            ])
            ->simplePaginate($perPage, ['*'], 'page', $page);
        });

        GoogleTagManager::browse('event', $data['events']->toArray()['data']);

        $title = 'Event';
        $description = 'Event Akan Datang dan Yang Telah Berlalu';

        SEO::setTitle($title);
        SEO::setDescription($description);

        return view('event.index')
            ->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        GoogleTagManager::set('pageType', ['type' => 'product', 'name' => 'event', 'view' => 'create event']);
        SEO::setTitle('New Event');
        SEO::setDescription('');
        $data['publicId'] = Hashids::connection('event')->encode(now()->timestamp);
        $data['category'] = collect(Category::orderBy('name', 'ASC')->pluck('name', 'id'));

        $data['facebook'] = null;

        if (!empty(auth()->user()->facebook_id)) {
            $data = array_merge($data, $this->getFacebookPage($this->api));
        }

        return view('event.create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(\App\Http\Requests\StoreEvent $request)
    {
        $data = $request->validated();
        $temps = explode(',', $data['tag']);

        foreach ($temps as &$temp) {
            $temp = trim($temp);
            Tag::firstOrCreate([
                'name' => $temp,
            ]);
        }

        $tags = Tag::whereIn('name', $temps)
            ->pluck('id')
            ->toArray();

        $last = Event::insertGetId([
            'name' => $data['name'],
            'category_id' => $data['category_id'],
            'place' => $data['place'],
            'lat' => $data['lat'],
            'lng' => $data['lng'],
            'user_id' => $request->user()['id'],
            'begin_at' => Carbon::parse($data['begin_at'])->toDateTimeString(),
            'end_at' => Carbon::parse($data['end_at'])->toDateTimeString(),
            'created_at' => Carbon::parse($data['created_at'])->toDateTimeString(),
        ]);

        $event = Event::findOrFail($last);
        $event->tags()->attach($tags);

        if (!Cloudder::rename('event/'.$data['public_id'], 'event/'.$event['hashed_id'])) {
            \Illuminate\Support\Facades\Log::error('Cannot Rename Image From ' . 'event/'.$data['public_id'] . ' TO ' . 'event/'.$event['hashed_id']);
        }

        Storage::put('event/'.$event['hashed_id'].'.md', $data['description']);

        for ($i=0; $i < 5; $i++) {
            Cache::forget('events_p_'.$i);
        }
        Cache::forget('event6');
        Cache::forget('moreEvent3');
        Cache::forget('upcoming_2events');
        foreach ($temps as $tag) {
            $slug = str_slug(strtolower($tag));

            if(Cache::has('tag_'. $slug)){
                Cache::forget('tag_'. $slug);
                Cache::forget('event_'. $slug);
            }
        }

        if(!empty($data['postFacebook'])){
            \App\Jobs\SocialShare::dispatch($this->api,
            action('EventController@show', ['id' => $event['hashed_id'], 'name' => $event['slug']]),
            $data['postFacebook'],
            Carbon::parse($data['created_at'])->timestamp);

        }
        return redirect('event');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Event $event
     *
     * @return \Illuminate\Http\Response
     */
    public function show($event)
    {
        $data['event'] = Event::findOrFail(Hashids::connection('event')->decode($event)[0]);

        $tags = [];
        foreach ($data['event']['tags'] as $tag) {
            array_push($tags, $tag['name']);
        }

        $link = Link::create($data['event']['name'], $data['event']['begin_at'], $data['event']['end_at'])
        ->description(str_limit($data['event']['description'], 50))
        ->address($data['event']->place);

        $data['iCal'] = $link->ics();
        $data['gCal'] = $link->google();
        $data['moreEvent'] = Cache::get('moreEvent3', function () use ($data) {
            $ttl = now()->addMonth();
            $cached = Event::latest('end_at')
            ->limit(3)
            ->whereNotIn('id', [$data['event']['id']])
            ->whereDate('begin_at', '>=', now())
            ->where(function ($query) {
                $query->whereDate('begin_at', '>=', now())
                        ->orwhereDate('end_at', '<=', now());
            })
            ->get();

            Cache::add('moreEvent3', $cached, $ttl);

            return $cached;
        });

        GoogleTagManager::set('pageType', [
            'type' => 'productDetail',
            'name' => 'event',
            'page_id' => $event,
            'page_name' => $data['event']['name']
        ]);

        GoogleTagManager::set('event', 'view_item');

        GoogleTagManager::set('ecommerce.event.product.details', [
            [
                'name' => $data['event']->name,
                'id' => $data['event']->hashed_id,
                'price' => 1,
                'brand' => $data['event']['user']->name,
                'geo' => $data['event']->place,
                'category' => $data['event']['category']->name,
            ],
        ]);

        SEO::setTitle($data['event']['name']);
        SEO::setDescription(str_limit($data['event']['description'], 50));
        SEO::metatags()->addMeta('article:published_time', $data['event']['created_at'], 'property');
        SEO::opengraph()->addProperty('type', 'articles');
        SEO::opengraph()->setArticle([
            'published_time' => $data['event']['created_at'],
            'modified_time' => $data['event']['created_at'],
            'expiration_time' => $data['event']['end_at'],
            'author' => $data['event']['user']['name'],
            'section' => 'class',
            'tag' => $tags,
        ]);
        SEO::setCanonical(url("/event/$event"));
        SEO::addImages($data['event']['url_u']);
        return view('event.show')
            ->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Event $event
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($event)
    {
        $data['event'] = Event::findOrFail(Hashids::connection('event')->decode($event)[0]);
        $this->authorize('update', $data['event']);
        $tags = [];
        foreach ($data['event']['tags'] as $tag) {
            array_push($tags, $tag['name']);
        }

        $data['tag'] = implode(',', $tags);

        GoogleTagManager::set('pageType', ['type' => 'product', 'name' => 'event', 'view' => 'edit event']);
        SEO::setTitle('Edit Event');
        $data['category'] = Category::orderBy('name', 'ASC')->pluck('name', 'id')->toArray();

        return view('event.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Event               $event
     *
     * @return \Illuminate\Http\Response
     */
    public function update(\App\Http\Requests\UpdateEvent $request, $event)
    {
        // Retrieve the validated input data...
        $data = $request->validated();
        $temps = explode(',', $data['tag']);

        foreach ($temps as &$temp) {
            $temp = trim($temp);
            Tag::firstOrCreate([
                'name' => $temp,
            ]);
        }

        $tags = Tag::whereIn('name', $temps)
            ->pluck('id')
            ->toArray();

        $last = Event::where('id', $data['id'])
        ->update([
            'name' => $data['name'],
            'category_id' => $data['category_id'],
            'place' => $data['place'],
            'lat' => $data['lat'],
            'lng' => $data['lng'],
            'user_id' => $request->user()['id'],
            'begin_at' => Carbon::parse($data['begin_at'])->toDateTimeString(),
            'end_at' => Carbon::parse($data['end_at'])->toDateTimeString(),
        ]);

        $event = Event::findOrFail($last);
        $event->tags()->detach();
        $event->tags()->attach($tags);

        $publicId = Hashids::connection('event')->encode($event->id);

        Storage::put('event/'.$publicId.'.md', $data['description']);

        Cache::forget('event6');
        Cache::forget('moreEvent3');

        return redirect('event');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Event $event
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($event)
    {
        $data['event'] = Event::findOrFail(Hashids::connection('event')->decode($event)[0]);
        $this->authorize('delete', $data['event']);
        Event::where('id' , $data['event']->id)->delete();
    }
}
