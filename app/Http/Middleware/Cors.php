<?php

namespace App\Http\Middleware;

use Closure;

class Cors
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request)
            ->header('Access-Control-Allow-Origin', "https://co-learningspace.web.id, https://sentry.io, https://www.googletagmanager.com, https://www.google-analytics.com")
            ->header('Access-Control-Allow-Methods', 'GET, POST');
            // ->header('Access-Control-Allow-Headers', 'Origin, Content-Type, Accept, Authorization, X-Request-With, X-Auth-Token');
    }
}
