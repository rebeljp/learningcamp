<?php

namespace App\Http\Middleware;

use Closure;

class SentryContext
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (app()->bound('sentry')) {
            //@var \Raven_Client $sentry
            $sentry = app('sentry');

            // Add user context
            $sentry->user_context(['id' => null]);
            if (auth()->check()) {
                $sentry->user_context([
                    'id' => auth()->id(),
                ]);
            }

            // Add tags context
            $sentry->tags_context(['runtime.type' => 'MiddleWare']);
        }

        return $next($request);
    }
}
