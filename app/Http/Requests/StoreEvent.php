<?php

namespace App\Http\Requests;

use App\Event;
use Auth;
use Illuminate\Foundation\Http\FormRequest;

class StoreEvent extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user()->can('create', Event::class);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'public_id' => 'required',
            'category_id' => 'required|exists:categories,id',
            'description' => 'required',
            'place' => 'required',
            'lat' => 'required|numeric|min:-90|max:90',
            'lng' => 'required|numeric|min:-180|max:180',
            'tag' => 'required',
            'begin_at' => 'required|date_format:Y-m-d H:i:s',
            'end_at' => 'required|date_format:Y-m-d H:i:s|after:begin_at',
            'created_at' => 'required|date_format:Y-m-d H:i:s'
        ];
    }
}
