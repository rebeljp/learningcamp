<?php

namespace App\Console\Commands;

use App;
use App\Classroom;
use App\Event;
use Illuminate\Console\Command;

class GenerateSitemaps extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sitemap:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Sitemap for Search Engine';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // create new sitemap object
        $sitemap = App::make('sitemap');

        // get all posts from db
        $Classrooms = Classroom::orderBy('begin_at')->get();
        $sitemap->add(url('/register'), '2017-01-01T07:00:00+07:00', '1.0', 'monthly');
        $sitemap->add(url('/password/reset'), '2017-01-01T07:00:00+07:00', '1.0', 'monthly');
        $sitemap->add(url('/contact-us'), '2017-01-01T07:00:00+07:00', '1.0', 'monthly');
        $sitemap->add(url('/class'), '2017-01-01T07:00:00+07:00', '1.0', 'daily');
        $sitemap->add(url('/event'), '2017-01-01T07:00:00+07:00', '1.0', 'daily');

        $counter = 3;
        $sitemapCounter = 0;

        $bar = $this->output->createProgressBar(count($Classrooms));
        // add every product to multiple sitemaps with one sitemap index
        foreach ($Classrooms as $class) {
            if (50000 == $counter) {
                // generate new sitemap file
                $sitemap->store('xml', 'sitemap-'.$sitemapCounter);
                // add the file to the sitemaps array
                $sitemap->addSitemap(secure_url('sitemap-'.$sitemapCounter.'.xml'));
                // reset items array (clear memory)
                $sitemap->model->resetItems();
                // reset the counter
                $counter = 0;
                // count generated sitemap
                ++$sitemapCounter;
            }

            // add product to items array
            $sitemap->add(url('/class/'.$class->hashed_id.'/'.$class['slug']), $class->created_at, '1.0', 'weekly');
            // count number of elements
            ++$counter;
            $bar->advance();
        }

        $this->info('Finish Creating Class');
        unset($Classrooms);

        $Events = Event::orderBy('begin_at')->get();
        $bar = $this->output->createProgressBar(count($Events));
        // add every product to multiple sitemaps with one sitemap index
        foreach ($Events as $event) {
            if (50000 == $counter) {
                // generate new sitemap file
                $sitemap->store('xml', 'sitemap-'.$sitemapCounter);
                // add the file to the sitemaps array
                $sitemap->addSitemap(secure_url('sitemap-'.$sitemapCounter.'.xml'));
                // reset items array (clear memory)
                $sitemap->model->resetItems();
                // reset the counter
                $counter = 0;
                // count generated sitemap
                ++$sitemapCounter;
            }
            $sitemap->add(url('/event/'.$event->hashed_id.'/'.$event['slug']), $event->created_at, '1.0', 'weekly');
            // count number of elements
            ++$counter;
            $bar->advance();
        }

        $this->info('Finish Creating Event');
        unset($Events);
        // you need to check for unused items
        if (! empty($sitemap->model->getItems())) {
            // generate sitemap with last items
            $sitemap->store('xml', 'sitemap-'.$sitemapCounter);
            // add sitemap to sitemaps array
            $sitemap->addSitemap(secure_url('sitemap-'.$sitemapCounter.'.xml'));
            // reset items array
            $sitemap->model->resetItems();
        }

        // generate new sitemapindex that will contain all generated sitemaps above
        $sitemap->store('sitemapindex', 'sitemap');
        $this->info('Sitemap Created');
    }
}
