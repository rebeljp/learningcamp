<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Scout\Searchable;

class Category extends Model
{
    use Searchable;
    use SoftDeletes;

    protected $guarded = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * Get the Location associated with the Event.
     */
    public function events()
    {
        return $this->hasMany(\App\Event::class)->published();
    }

    /**
     * Get the Location associated with the Event.
     */
    public function classrooms()
    {
        return $this->hasMany(\App\Classroom::class)->published();
    }
}
