<?php

namespace App\Providers;

use App\Observers\UserObserver;
use App\User;
use Carbon\Carbon;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use OpenGraph;
use SEOMeta;
use Twitter;
use URL;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        $yearCopy = now()->year;
        view()->share('yearCopy', $yearCopy);
        User::observe(UserObserver::class);
        SEOMeta::setKeywords([
            'cls', 'learning', 'space',
            'tempat', 'belajar', 'event',
            'diskusi', 'mentoring', 'mentor',
        ]);

        OpenGraph::setUrl(url()->full());
        OpenGraph::addProperty('locale', 'id-ID');

        Twitter::setSite('@CLSxID'); // site of twitter card tag
        Twitter::setUrl(url()->full()); // url of twitter card tag

        Route::resourceVerbs([
            'create' => 'buat',
            'edit' => 'ubah',
            'hapus' => 'hapus',
        ]);
        if (! app()->environment('local')) {
            URL::forceScheme('https');
        }
    }

    /**
     * Register any application services.
     */
    public function register()
    {
    }
}
