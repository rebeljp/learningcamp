<?php

namespace App\Providers;

use App\Classroom;
use App\Event;
use App\Policies\ClassPolicy;
use App\Policies\EventPolicy;
use App\Policies\UserPolicy;
use App\User;
use Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        User::class => UserPolicy::class,
        Event::class => EventPolicy::class,
        Classroom::class => ClassPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     */
    public function boot()
    {
        $this->registerPolicies();
        Gate::resource('user', \App\Policies\UserPolicy::class);
        Gate::resource('event', \App\Policies\EventPolicy::class);
        Gate::resource('class', \App\Policies\ClassPolicy::class);
    }
}
