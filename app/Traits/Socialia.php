<?php

namespace App\Traits;

use Facebook\Facebook;

trait Socialia
{
    public function getFacebookPage($api){
        try {
            $params = "id,name";
            $response = $api->get('/me/accounts', auth()->user()->facebook_id);
            $pages = $response->getGraphEdge()->asArray();

            $data['selected'] = $pages[0]['id'];
            $data['facebook'] = [];

            foreach ($pages as $page) {
                $data['facebook'] = array_add($data['facebook'], $page['id'], $page['name']);
            }
            return $data;
        } catch (\Facebook\Exceptions\FacebookSDKException $e) {
        }
    }

    public function publishToPage($api, $url, $page_id, $publish_at){

        try {
            $post = $api->post('/' . $page_id . '/feed', [
                'link' => $url,
                "published" => false,
                'scheduled_publish_time' => $publish_at
            ], $this->getPageAccessToken($page_id));

            return $post->getGraphNode()->asArray();

        } catch (\Facebook\Exceptions\FacebookSDKException $e) {
            return $e->getMessage(); // handle exception
        }
    }

    public function getPageAccessToken($page_id){
        try {
            // Get the \Facebook\GraphNodes\GraphUser object for the current user.
            // If you provided a 'default_access_token', the '{access-token}' is optional.
            $response = $this->api->get('/me/accounts', auth()->user()->facebook_id);
        } catch(\Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            return $e->getMessage();
        } catch(\Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            return $e->getMessage();
        }

        try {
            $pages = $response->getGraphEdge()->asArray();
            foreach ($pages as $key) {
                if ($key['id'] == $page_id) {
                    return $key['access_token'];
                }
            }
        } catch (\Facebook\Exceptions\FacebookSDKException $e) {
            return $e->getMessage();
        }
    }

}
