<?php

return [
    'meta' => [
        /*
         * The default configurations to be used by the meta generator.
         */
        'defaults' => [
            'title' => 'Co-Learning Space', // set false to total remove
            'description' => 'Find your study buddy, whom you can learn something new with.', // set false to total remove
            'separator' => ' - ',
            'keywords' => ['CLS', 'Classroom', 'Belajar', 'kelas', 'event', 'bandung', 'indonesia', 'seminar', 'workshop'],
            'canonical' => null, // Set null for using Url::current(), set false to total remove
        ],

        /*
         * Webmaster tags are always added.
         */
        'webmaster_tags' => [
            'google' => 'vk7yw1OIykNPFhUfYS-16tUr7CfiR7n8TS669m9_Cwo',
            'bing' => 'B266928EA7A016008B68D9107E33C8EC',
            'alexa' => null,
            'pinterest' => null,
            'yandex' => '8e7a5bcc603e3083',
        ],
    ],
    'opengraph' => [
        /*
         * The default configurations to be used by the opengraph generator.
         */
        'defaults' => [
            'title' => 'Co-Learning Space', // set false to total remove
            'description' => 'Find your study buddy, whom you can learn something new with.', // set false to total remove
            'url' => null, // Set null for using Url::current(), set false to total remove
            'type' => 'website',
            'site_name' => 'Co-Learning Space',
            'images' => [],
        ],
    ],
    'twitter' => [
        /*
         * The default values to be used by the twitter cards generator.
         */
        'defaults' => [
            //'card'        => 'summary',
            //'site'        => '@LuizVinicius73',
        ],
    ],
];
