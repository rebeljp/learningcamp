<?php

return [
    /*
     * The Google Tag Manager id, should be a code that looks something like "gtm-xxxx".
     */
    'id' => 'GTM-MX832T7',

    /*
     * Enable or disable script rendering. Useful for local development.
     */
    'enabled' => 'production' === env('APP_ENV'),

    /*
     * If you want to use some macro's you 'll probably store them
     * in a dedicated file. You can optionally define the path
     * to that file here and we will load it for you.
     */
    'macroPath' => app_path('Helpers/Browse.php'),

    /*
     * The key under which data is saved to the session with flash.
     */
    'sessionKey' => '_googleTagManager',
];
