<?php

return [
    'dsn' => env('MIX_SENTRY_DSN'),

    // capture release as git sha
    'release' => env('MIX_APP_HASH'),

    // Capture bindings on SQL queries
    'breadcrumbs.sql_bindings' => true,

    // Capture default user context
    'user_context' => true,
];
