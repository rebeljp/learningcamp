<?php

return [
    /*
     * Server
     *
     * Reference: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Server
     *
     * Note: when server is empty string, it will not add to response header
     */

    'server' => 'Narberal',

    'X-Powered-By' => 'Lower Life Form',

    /*
     * X-Content-Type-Options
     *
     * Reference: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Content-Type-Options
     *
     * Available Value: 'nosniff'
     */

    'x-content-type-options' => 'nosniff',

    /*
     * X-Download-Options
     *
     * Reference: https://msdn.microsoft.com/en-us/library/jj542450(v=vs.85).aspx
     *
     * Available Value: 'noopen'
     */

    'x-download-options' => 'noopen',

    /*
     * X-Frame-Options
     *
     * Reference: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Frame-Options
     *
     * Available Value: 'deny', 'sameorigin', 'allow-from <uri>'
     */

    'x-frame-options' => 'sameorigin',

    /*
     * X-Permitted-Cross-Domain-Policies
     *
     * Reference: https://www.adobe.com/devnet/adobe-media-server/articles/cross-domain-xml-for-streaming.html
     *
     * Available Value: 'all', 'none', 'master-only', 'by-content-type', 'by-ftp-filename'
     */

    'x-permitted-cross-domain-policies' => 'none',

    /*
     * X-XSS-Protection
     *
     * Reference: https://blogs.msdn.microsoft.com/ieinternals/2011/01/31/controlling-the-xss-filter
     *
     * Available Value: '1', '0', '1; mode=block'
     */

    'x-xss-protection' => '1; mode=block',

    /*
     * Referrer-Policy
     *
     * Reference: https://w3c.github.io/webappsec-referrer-policy
     *
     * Available Value: 'no-referrer', 'no-referrer-when-downgrade', 'origin', 'origin-when-cross-origin',
     *                  'same-origin', 'strict-origin', 'strict-origin-when-cross-origin', 'unsafe-url'
     */

    'referrer-policy' => 'strict-origin-when-cross-origin',

    /*
     * Clear-Site-Data
     *
     * Reference: https://w3c.github.io/webappsec-clear-site-data/
     */

    'clear-site-data' => [
        'enable' => false,

        'all' => false,

        'cache' => true,

        'cookies' => true,

        'storage' => true,

        'executionContexts' => true,
    ],

    /*
     * HTTP Strict Transport Security
     *
     * Reference: https://developer.mozilla.org/en-US/docs/Web/Security/HTTP_strict_transport_security
     *
     * Please ensure your website had set up ssl/tls before enable hsts.
     */

    'hsts' => [
        'enable' => 'production' === env('APP_ENV'),

        'max-age' => 31536000,

        'include-sub-domains' => true,
    ],

    /*
     * Expect-CT
     *
     * Reference: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Expect-CT
     */

    'expect-ct' => [
        'enable' => 'production' === env('APP_ENV'),

        'max-age' => 2147483648,

        'enforce' => true,

        'report-uri' => env('SENTRY_SECURE_HEADER'),
    ],

    /*
     * Public Key Pinning
     *
     * Reference: https://developer.mozilla.org/en-US/docs/Web/Security/Public_Key_Pinning
     *
     * hpkp will be ignored if hashes is empty.
     */

    'hpkp' => [
        'hashes' => [
            'ppuO6EAizqN6SSAPjbHcHigFuNQPq3dA4oBm4v5pVzQ=',
            'YLh1dUR9y6Kja30RrAn7JKnbQG/uEtLMkBgFF2Fuihg=',
            'Vjs8r4z+80wjNcr1YKepWQboSIRi63WsWXhIMN+eWys='
        ],

        'include-sub-domains' => true,

        'max-age' => 7257600,

        'report-only' => true,

        'report-uri' => env('SENTRY_SECURE_HEADER'),
    ],

    /*
     * Feature Policy
     *
     * Reference: https://wicg.github.io/feature-policy/
     */

    'feature-policy' => [
        'enable' => true,

        /*
         * Each directive details can be found on:
         *
         * https://github.com/WICG/feature-policy/blob/master/features.md
         *
         * 'none', '*' and 'self allow' are mutually exclusive,
         * the priority is 'none' > '*' > 'self allow'.
         */

        'camera' => [
            'none' => false,

            '*' => false,

            'self' => true,

            'allow' => [
                // 'url',
            ],
        ],

        'fullscreen' => [
            'none' => false,

            '*' => false,

            'self' => true,

            'allow' => [
                // 'url',
            ],
        ],

        'geolocation' => [
            'none' => false,

            '*' => false,

            'self' => true,

            'allow' => [
                // 'url',
            ],
        ],

        'microphone' => [
            'none' => false,

            '*' => false,

            'self' => true,

            'allow' => [
                // 'url',
            ],
        ],

        'midi' => [
            'none' => false,

            '*' => false,

            'self' => true,

            'allow' => [
                // 'url',
            ],
        ],

        'payment' => [
            'none' => false,

            '*' => false,

            'self' => true,

            'allow' => [
                // 'url',
            ],
        ],

        'picture-in-picture' => [
            'none' => false,

            '*' => true,

            'self' => false,

            'allow' => [
                // 'url',
            ],
        ],

        'accelerometer' => [
            'none' => false,

            '*' => false,

            'self' => true,

            'allow' => [
                // 'url',
            ],
        ],

        'ambient-light-sensor' => [
            'none' => false,

            '*' => false,

            'self' => true,

            'allow' => [
                // 'url',
            ],
        ],

        'gyroscope' => [
            'none' => false,

            '*' => false,

            'self' => true,

            'allow' => [
                // 'url',
            ],
        ],

        'magnetometer' => [
            'none' => false,

            '*' => false,

            'self' => true,

            'allow' => [
                // 'url',
            ],
        ],

        'speaker' => [
            'none' => false,

            '*' => false,

            'self' => true,

            'allow' => [
                // 'url',
            ],
        ],

        'usb' => [
            'none' => false,

            '*' => false,

            'self' => true,

            'allow' => [
                // 'url',
            ],
        ],

        'vr' => [
            'none' => false,

            '*' => false,

            'self' => true,

            'allow' => [
                // 'url',
            ],
        ],
    ],

    /*
     * Content Security Policy
     *
     * Reference: https://developer.mozilla.org/en-US/docs/Web/Security/CSP
     *
     * csp will be ignored if custom-csp is not null. To disable csp, set custom-csp to empty string.
     *
     * Note: custom-csp does not support report-only.
     */

    'custom-csp' => '',

    'csp' => [
        'report-only' => true, //'local' === env('APP_ENV'),

        'report-uri' => env('SENTRY_SECURE_HEADER'),

        'block-all-mixed-content' => true,

        'upgrade-insecure-requests' => false,

        /*
         * Please references script-src directive for available values, only `script-src` and `style-src`
         * supports `add-generated-nonce`.
         *
         * Note: when directive value is empty, it will use `none` for that directive.
         */

        'script-src' => [
            'allow' => [
                'https://www.googletagmanager.com',
                'https://www.google-analytics.com',
                'https://widget.cloudinary.com',
                'https://maps.googleapis.com',
                'https://connect.facebook.net',

                'https://pagead2.googlesyndication.com',
                'https://adservice.google.co.id',
                'https://adservice.google.com',
                'https://amp.cloudflare.com'
            ],

            'hashes' => [
                'sha256' => [
                    'sha256-v2XDaBAw2SXTwK7wfVHZkvezBOPqr8hlDHejcDVisBA=',
                    'sha256-RHEMsc1XWj6IpQ1d3JnaudFT8ZzcHq2RlmRhSpejXE4='
                ],
            ],

            'nonces' => [
                // 'base64-encoded',
            ],

            'schemes' => [
                'data:',
                'inline:'
            ],

            'self' => true,

            'unsafe-inline' => false,

            'unsafe-eval' => false,

            'strict-dynamic' => false,

            'unsafe-hashed-attributes' => false,

            'add-generated-nonce' => true,
        ],

        'style-src' => [
            'allow' => [
                //
            ],

            'hashes' => [
                'sha256' => [
                    'sha256-pd32RWI3nNZOaUZucIL+rJ9CV3m/SJ6hEpCQhHNd9uU=',
                ],
            ],

            'nonces' => [
                //
            ],

            'schemes' => [
                'https:',
            ],

            'self' => true,

            'unsafe-inline' => false,

            'add-generated-nonce' => true,
        ],

        'img-src' => [
            'allow' => [
                'https://res.cloudinary.com',
                'https://www.google-analytics.com',
                'https://www.google.co.id',
                'https://www.google.com',
                'https://stats.g.doubleclick.net',
            ],

            'schemes' => [
                'data:',
                'inline:'
            ],

            'self' => true,
        ],

        'default-src' => [
            'self' => true,
        ],

        'base-uri' => [
            'self' => true,
        ],

        'connect-src' => [
            'allow' => [
                'https://sentry.io',
            ],

            'self' => true,
        ],

        'font-src' => [
            'self' => true,

            'schemes' => [
                'data:'
            ],
        ],

        'form-action' => [
            'self' => true,
        ],

        'frame-ancestors' => [
            //
        ],

        'frame-src' => [
            'allow' => [
                'https://www.googletagmanager.com',
                'https://www.youtube.com',
                'https://res.cloudinary.com',
                'https://staticxx.facebook.com/',
                'https://www.facebook.com/',
                'https://googleads.g.doubleclick.net'
            ],

        ],

        'manifest-src' => [
            'self' => true,
        ],

        'media-src' => [
            'self' => true,
        ],

        'object-src' => [
            //
        ],

        'worker-src' => [
            //
        ],

        'plugin-types' => [
            // 'application/x-shockwave-flash',
        ],

        'require-sri-for' => '',

        'sandbox' => '',

    ],

];
