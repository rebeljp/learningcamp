<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Event;
use App\Classroom;

class DataTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic test example.
     * @return void
     */
    public function testInsert()
    {
        factory(\App\User::class, 15)->create();

        // Users Type and Their Creation
        $this->assertTrue(!empty(\App\User::with(['role', 'events', 'classrooms'])->get()));

        // Creations With Relation
        $this->assertTrue(!empty(Event::with(['user', 'tags', 'category'])->get()));

        $this->assertTrue(!empty(Classroom::with(['user', 'tags', 'category'])->get()));

        // Update Event
        $event = Event::first();
        $event->update([
            'place' => 'Kuburan'
        ]);
        $this->assertDatabaseHas('events', [
            'id' => $event->id,
            'place' => 'Kuburan'
        ]);

        // Update Class
        $class = Classroom::first();
        $class->update([
            'place' => 'Kuburan'
        ]);
        $this->assertDatabaseHas('classrooms', [
            'id' => $class->id,
            'place' => 'Kuburan'
        ]);

        // Delete Event
        $event->delete();
        $this->assertSoftDeleted('events', [
            'id' => $event->id
        ]);

        // Delete Class
        $class->delete();
        Classroom::where('id', 13)->delete();
        $this->assertSoftDeleted('classrooms', [
            'id' => $class->id
        ]);

        // Delete User
        $user = \App\User::first();
        $user->delete();
        $this->assertSoftDeleted('users', [
            'id' => $user->id
        ]);
    }
}
