<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RoleTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic test example.
     *
     * @return void
     */

    public function testRole()
    {
        factory(\App\Role::class, 5)->create();
        factory(\App\User::class)->create();
        // Role with Users
        $this->assertTrue(!empty(\App\Role::with('user')->get()));

        // Update Role
        \App\Role::where('name', 'Super Admin')->first()->update([
            'description' => 'Super Saiyan'
        ]);
        $this->assertDatabaseHas('roles', [
            'name' => 'Super Admin',
            'description' => 'Super Saiyan'
        ]);

        // Delete Tag
        $role = \App\Role::inRandomOrder()->first();
        $role->delete();
        $this->assertSoftDeleted('roles', [
            'id' => $role->id
        ]);
    }
}
