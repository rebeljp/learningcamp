<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Artisan;

class ArtisanTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        factory(\App\User::class, 15)->create();
        $this->artisan('sitemap:generate')
            ->expectsOutput('Finish Creating Class')
            ->expectsOutput('Finish Creating Event')
            ->expectsOutput('Sitemap Created')
            ->assertExitCode(0);
    }
}
