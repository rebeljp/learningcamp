<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CategoryTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCategory()
    {
        factory(\App\Category::class, 15)->create();
        factory(\App\User::class)->create();

        // Tags find Creation
        $this->assertTrue(!empty(\App\Category::with(['events', 'classrooms'])->get()));

        // Update Category
        \App\Category::first()->update([
            'name' => 'Sejarah Game'
        ]);
        $this->assertDatabaseHas('categories', [
            'name' => 'Sejarah Game'
        ]);

        // Delete Tag
        $cate = \App\Category::inRandomOrder()->first();
        $cate->delete();
        $this->assertSoftDeleted('categories', [
            'id' => $cate->id
        ]);
    }
}
