<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TagTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testTag()
    {
        factory(\App\Tag::class, 15)->create();
        factory(\App\User::class)->create();

        // Tags find Creation
        $this->assertTrue(!empty(\App\Tag::with(['events', 'classrooms'])->get()));

        // Update Tag
        \App\Tag::first()->update([
            'name' => 'CLS'
        ]);
        $this->assertDatabaseHas('tags', [
            'name' => 'CLS',
        ]);

        // Delete Tag
        $tag = \App\Tag::inRandomOrder()->first();
        $tag->delete();
        $this->assertSoftDeleted('tags', [
            'id' => $tag->id
        ]);
    }
}
