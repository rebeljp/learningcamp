<?php

namespace Tests\Feature;

use Mockery;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Socialite\Facades\Socialite;

class UserPageTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test User Profile Page.
     * @covers App\Http\Controllers\ProfileInvoker::__invoke
     *
     */
    public function testProfilePage()
    {
        $this->withoutExceptionHandling();
        $user = factory(\App\User::class)->create();

        $response = $this->actingAs($user)->get('/user/' . $user->hashed_id . '/profile');

        $response->assertSee('Degree')
            ->assertSee('contact info')
            ->assertSee('Chat');
    }

    /**
     * Test User Profile Page.
     * @depends testProfilePage
     * @covers App\Http\Controllers\UserController::edit
     *
     */
    public function testUserSettingPage()
    {
        $this->withoutExceptionHandling();
        $user = factory(\App\User::class)->create();

        $response = $this->actingAs($user)
                ->get('/user/' . $user->hashed_id . '/ubah');

        $response->assertSee('User Settings')
            ->assertSee('Profile')
            ->assertSee('Integration')
            ->assertSee('Notification');
    }

    /**
     * Test Admin Page.
     * @depends testProfilePage
     * @covers App\Http\Controllers\AdminController::index
     *
     */
    public function testAdminPage()
    {
        $this->withoutExceptionHandling();
        factory(\App\Role::class, 5)->create();
        $user = factory(\App\User::class)->states('supa_admin')->create();

        $response = $this->actingAs($user)->get('/admin');

        $response->assertSee('Simple Admin Page')
            ->assertSee('Manage Category')
            ->assertSee('Manage Role');
    }

    /**
     * Test Admin Page.
     * @depends testProfilePage
     * @covers App\Http\Controllers\SocialiteController::handleProviderFacebookCallback
     *
     */
    public function testFacebookPage()
    {
        $abstractUser = Mockery::mock('Laravel\Socialite\Two\User');
        $abstractUser->shouldReceive('getId')
            ->andReturn(1234567890)
            ->shouldReceive('getEmail')
            ->andReturn(str_random(10).'@test.com')
            ->shouldReceive('getNickname')
            ->andReturn('Pseudo')
            ->shouldReceive('getName')
            ->andReturn('Arlette Laguiller')
            ->shouldReceive('getAvatar')
            ->andReturn('https://en.gravatar.com/userimage');

        $provider = Mockery::mock('Laravel\Socialite\Contracts\Provider');
        $provider->shouldReceive('user')->andReturn($abstractUser);

        Socialite::shouldReceive('driver')->with('facebook')->andReturn($provider);

        $this->get(route("authFacebookCallback"))
            ->assertSee(route("home"));
    }
}
