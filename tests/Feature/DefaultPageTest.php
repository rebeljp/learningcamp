<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DefaultPageTest extends TestCase
{

    use RefreshDatabase;
    /**
     * A basic test example. Check homepage
     * @covers App\Http\Controllers\HomeController::index
     *
     */
    public function testBasicTest()
    {
        factory(\App\User::class)->create();
        $this->withoutExceptionHandling();
        $response = $this->get('/');

        $response->assertOk()
            ->assertSee("Latest Class")
            ->assertSee("Latest Event")
            ->assertSee("Find your study buddy, whom you can learn something new with.");
    }

    /**
     * Test Login Page.
     * @depends testBasicTest
     *
     */
    public function testLoginPage()
    {
        $response = $this->get('/login');

        $response->assertOk()
            ->assertSee("Welcome Back")
            ->assertSee("Remember Me");
    }

    /**
     * Test Register Page.
     * @depends testBasicTest
     *
     */
    public function testRegisterPage()
    {
        $response = $this->get('/register');

        $response->assertOk()
            ->assertSee("Register New Account")
            ->assertSee("Register");
    }

    /**
     * Test reset Page.
     * @depends testBasicTest
     *
     */
    public function testResetPage()
    {
        $response = $this->get('/password/reset');

        $response->assertOk()
            ->assertSee("Lost your password?")
            ->assertSee("Reset Password");
    }

    /**
     * Test Contact Us Page.
     * @depends testBasicTest
     * @covers App\Http\Controllers\HomeController::contact
     *
     */
    public function testContactUsPage()
    {
        $response = $this->get('/contact-us');

        $response->assertOk()
            ->assertSee("section-contact");
    }

    /**
     * Test About Us Page.
     * @depends testBasicTest
     * @covers App\Http\Controllers\HomeController::about
     *
     */
    public function testAboutUsPage()
    {
        $response = $this->get('/about-us');

        $response->assertOk()
            ->assertSee("Co-Learning Space");
    }

    /**
     * Test privacy-policy Page.
     * @depends testBasicTest
     * @covers App\Http\Controllers\HomeController::privacy
     *
     */
    public function testPrivacyPage()
    {
        $response = $this->get('/privacy-policy');

        $response->assertOk()
            ->assertSee("Co-Learning Space");
    }

    /**
     * Test terms-of-use Page.
     * @depends testBasicTest
     * @covers App\Http\Controllers\HomeController::terms
     *
     */
    public function testTermsUsePage()
    {
        $response = $this->get('/terms-of-use');

        $response->assertOk()
            ->assertSee("Co-Learning Space");
    }
}
