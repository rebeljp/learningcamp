<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Storage;

class EventPageTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    /**
     * A basic test index.
     * @covers App\Http\Controllers\EventController::index
     *
     * @return void
     */
    public function testIndex()
    {
        factory(\App\User::class)->create();
        $this->withoutExceptionHandling();
        $response = $this->get('/event');

        $response->assertOk()
            ->assertDontSee('Bandung Open Education Day')
            ->assertDontSee("Create New Event");
    }

    /**
     * A basic test create.
     * @depends testIndex
     * @covers App\Http\Controllers\EventController::create
     *
     * @return void
     */
    public function testCreate()
    {
        $this->withoutExceptionHandling();
        $count = \App\Role::count();
        if (empty($count)) {
            factory(\App\Role::class, 5)->create();
        }
        $user = factory(\App\User::class)->states('supa_admin')->create();

        $response = $this->actingAs($user)->get('/event/buat');
        $response->assertSessionHasNoErrors()
            ->assertOk()
            ->assertSee('Create New Event');
    }
    /**
     * A basic test show.
     * @depends testIndex
     * @covers App\Http\Controllers\EventController::show
     *
     * @return void
     */
    public function testShow()
    {
        $this->withoutExceptionHandling();
        $count = \App\Role::count();
        if (empty($count)) {
            factory(\App\Role::class, 5)->create();
        }

        $user = factory(\App\User::class)->create();
        $event = $user->events()->first();

        Storage::put('event/' . $event->hashed_id . '.md', $this->faker->paragraphs(3, true));

        Storage::assertExists('event/' . $event->hashed_id . '.md');
        $response = $this->get('event/' . $event->hashed_id);

        $response->assertSessionHasNoErrors()
            ->assertOk()
            ->assertSee($event->name)
            ->assertSee($event->place)
            ->assertSee('Upcoming Event');
    }

    /**
     * A login test show.
     * @depends testIndex
     * @covers App\Http\Controllers\EventController::show
     *
     * @return void
     */

    public function testShowLogged()
    {
        $this->withoutExceptionHandling();
        $count = \App\Role::count();
        if(empty($count)){
            factory(\App\Role::class, 5)->create();
        }

        $user = factory(\App\User::class)->create();
        $event = $user->events()->first();

        Storage::put('event/' . $event->hashed_id . '.md', $this->faker->paragraphs(3, true));

        Storage::assertExists('event/' . $event->hashed_id . '.md');

        $response = $this->actingAs($user)
            ->assertAuthenticated()
            ->get('event/' . $event->hashed_id);

        $response->assertSessionHasNoErrors()
            ->assertOk()
            ->assertSee($event->name)
            ->assertSee($event->place)
            ->assertSee('Upcoming Event')
            ->assertSee('Edit Event');
    }

    /**
     * A basic test edit.
     * @depends testIndex
     * @covers App\Http\Controllers\EventController::edit
     *
     * @return void
     */
    public function testEdit()
    {
        $this->withoutExceptionHandling();
        $count = \App\Role::count();
        if(empty($count)){
            factory(\App\Role::class, 5)->create();
        }

        $user = factory(\App\User::class)->create();
        $event = $user->events()->first();

        Storage::put('event/' . $event->hashed_id . '.md', $this->faker->paragraphs(3, true));

        Storage::assertExists('event/' . $event->hashed_id . '.md');

        $response = $this->actingAs($user)
            ->assertAuthenticated()
            ->get('event/' . $event->hashed_id . '/ubah');

        $response->assertSessionHasNoErrors()
            ->assertOk()
            ->assertSee('Edit Event');
    }

}
