<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Storage;

class ClassPageTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    /**
     * A basic test index.
     * @covers App\Http\Controllers\ClassroomController::index
     *
     * @return void
     */
    public function testIndex()
    {
        factory(\App\User::class)->create();
        $this->withoutExceptionHandling();
        $response = $this->get('/class');

        $response->assertOk()
            ->assertDontSee('STUCO')
            ->assertDontSee("Create New Class");
    }

    /**
     * A basic test create.
     * @depends testIndex
     * @covers App\Http\Controllers\ClassroomController::create
     *
     * @return void
     */
    public function testCreate()
    {
        $this->withoutExceptionHandling();
        $count = \App\Role::count();
        if (empty($count)) {
            factory(\App\Role::class, 5)->create();
        }

        $user = factory(\App\User::class)->states('supa_admin')->create();

        $response = $this->actingAs($user)->get('/class/buat');
        $response->assertSessionHasNoErrors()
            ->assertOk()
            ->assertSee('Create New Class');
    }

    /**
     * A basic test show.
     * @depends testIndex
     * @covers App\Http\Controllers\ClassroomController::show
     *
     * @return void
     */
    public function testShow()
    {
        $this->withoutExceptionHandling();
        $count = \App\Role::count();
        if (empty($count)) {
            factory(\App\Role::class, 5)->create();
        }

        $user = factory(\App\User::class)->create();
        $class = $user->classrooms()->first();

        Storage::put('class/' . $class->hashed_id . '.md', $this->faker->paragraphs(3, true));

        Storage::assertExists('class/' . $class->hashed_id . '.md');
        $response = $this->get('class/' . $class->hashed_id);

        $response->assertSessionHasNoErrors()
            ->assertOk()
            ->assertSee($class->name)
            ->assertSee($class->place)
            ->assertSee('Upcoming Class');
    }

    /**
     * A login test show.
     * @depends testIndex
     * @covers App\Http\Controllers\ClassroomController::show
     *
     * @return void
     */

    public function testShowLogged()
    {
        $this->withoutExceptionHandling();
        $count = \App\Role::count();
        if(empty($count)){
            factory(\App\Role::class, 5)->create();
        }

        $user = factory(\App\User::class)->create();
        $class = $user->classrooms()->first();

        Storage::put('class/' . $class->hashed_id . '.md', $this->faker->paragraphs(3, true));

        Storage::assertExists('class/' . $class->hashed_id . '.md');

        $response = $this->actingAs($user)
            ->assertAuthenticated()
            ->get('class/' . $class->hashed_id);

        $response->assertSessionHasNoErrors()
            ->assertOk()
            ->assertSee($class->name)
            ->assertSee($class->place)
            ->assertSee('Upcoming Class')
            ->assertSee('Edit Class');
    }

    /**
     * A basic test edit.
     * @depends testIndex
     * @covers App\Http\Controllers\ClassroomController::edit
     *
     * @return void
     */
    public function testEdit()
    {
        $this->withoutExceptionHandling();
        $count = \App\Role::count();
        if (empty($count)) {
            factory(\App\Role::class, 5)->create();
        }

        $user = factory(\App\User::class)->create();
        $class = $user->classrooms()->first();

        Storage::put('class/' . $class->hashed_id . '.md', $this->faker->paragraphs(3, true));

        Storage::assertExists('class/' . $class->hashed_id . '.md');

        $response = $this->actingAs($user)
            ->assertAuthenticated()
            ->get('class/' . $class->hashed_id . '/ubah');

        $response->assertSessionHasNoErrors()
            ->assertOk()
            ->assertSee('Edit Class');
    }

}
