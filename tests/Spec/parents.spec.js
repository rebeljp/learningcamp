import { mount } from "@vue/test-utils";
require("@/fa");
import SectionParents from "@/components/SectionParents.vue";

test("it works", () => {
	expect(1 + 1).toBe(2);
});

test("should mount without crashing", () => {
	const wrapper = mount(SectionParents);

	expect(wrapper).toMatchSnapshot();
});
