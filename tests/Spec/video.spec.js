import { mount } from "@vue/test-utils";
require("@/fa");
import SectionVideo from "@/components/SectionVideo.vue";

test("it works", () => {
	expect(1 + 1).toBe(2);
});

test("should mount without crashing", () => {
	const wrapper = mount(SectionVideo);

	expect(wrapper).toMatchSnapshot();
});
