import { mount } from "@vue/test-utils";
import SectionWelcome from "@/components/SectionWelcome.vue";

test("it works", () => {
	expect(1 + 1).toBe(2);
});

test("should mount without crashing", () => {
	const wrapper = mount(SectionWelcome);

	expect(wrapper).toMatchSnapshot();
});
