import { mount } from "@vue/test-utils";
require("@/fa");
import SectionContact from "@/components/SectionContact.vue";

test("it works", () => {
	expect(1 + 1).toBe(2);
});

test("should mount without crashing", () => {
	const wrapper = mount(SectionContact);

	expect(wrapper).toMatchSnapshot();
});
