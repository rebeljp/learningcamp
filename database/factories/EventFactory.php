<?php

use Faker\Generator as Faker;
use Illuminate\Support\Facades\Storage;

$factory->define(App\Event::class, function (Faker $faker) {
    $start = $faker->dateTimeBetween('next Monday', 'next Monday +7 days');
    $end = $faker->dateTimeBetween($start, $start->format('Y-m-d H:i:s').' +2 days');

    factory(\App\Category::class, 2)->create();

    return [
        'name' => $faker->sentence(6, true),
        'category_id' => App\Category::inRandomOrder()->first(),
        'place' => $faker->streetName,
        'lat' => $faker->latitude(-90, 90),
        'lng' => $faker->longitude(-180, 180),
        'begin_at' => $start,
        'end_at'=> $end
    ];
});

$factory->afterCreating(App\Event::class, function (App\Event $event, Faker $faker) {
    // Add Tags
    for ($i = 0; $i < 6; $i++) {
        $event->tags()->attach(factory(App\Tag::class)->make());
    }

    Storage::persistentFake('local');
    Storage::put('event/'. $event->hashed_id . '.md', $faker->paragraphs(3, true));
});
