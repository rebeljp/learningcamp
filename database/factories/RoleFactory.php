<?php

use Faker\Generator as Faker;

$factory->define(App\Role::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->randomElement(['Super Admin', 'Admin', 'Venue', 'Trainer', 'Trainee']),
        'description' => $faker->paragraph(1, true)
    ];
});
