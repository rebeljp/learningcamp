<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => $faker->dateTime('now', 'Asia/Jakarta'),
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
        'handphone' => $faker->e164PhoneNumber,
        'organization' => $faker->company
    ];
});

$factory->afterCreating(App\User::class, function ($user, $faker) {
    // Add Role
    $role_employee = App\Role::inRandomOrder()->first();
    $user->role()->associate($role_employee);

    // Add 10 Event and Classes
    for ($i=0; $i < 5; $i++) {
        $user->events()->save(factory(App\Event::class)->make());
        $user->classrooms()->save(factory(App\Classroom::class)->make());
    }
});

$factory->state(App\User::class, 'supa_admin', function ($faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => $faker->dateTime('now', 'Asia/Jakarta'),
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
        'handphone' => $faker->e164PhoneNumber,
        'organization' => $faker->company
    ];
});

$factory->afterCreatingState(App\User::class, 'supa_admin', function ($user, $faker) {
    // Add Role
    $role_employee = App\Role::where('name', 'Super Admin')->first();
    $user->role()->associate($role_employee);

    // Add 10 Event and Classes
    for ($i=0; $i < 5; $i++) {
        $user->events()->save(factory(App\Event::class)->make());
        $user->classrooms()->save(factory(App\Classroom::class)->make());
    }
});
