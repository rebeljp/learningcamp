<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run()
    {
        $this->call(RoleTableSeeder::class);
        // User seeder will use the roles above created.
        $this->call(UserSeed::class);
        $this->call(CategorySeed::class);
        $this->call(EventSeed::class);
        $this->call(ClassroomSeed::class);
        $this->call(TagSeeder::class);
    }
}
