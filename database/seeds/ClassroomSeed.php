<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class ClassroomSeed extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        DB::table('classrooms')->insert([
            'name' => 'STUCO',
            'place' => 'Regional Secretariat Bandung',
            'lat' => '-6.9131982',
            'lng' => '107.6090273',
            'category_id' => 1,
            'begin_at' => Carbon::createFromFormat('Y-m-d H:i:s', '2018-03-03 9:00:00'),
            'end_at' => Carbon::createFromFormat('Y-m-d H:i:s', '2018-03-03 16:00:00'),
            'user_id' => 1,
        ]);

        DB::table('classrooms')->insert([
            'name' => 'Crash Course to Scrum',
            'place' => 'Spasial',
            'lat' => '-6.916586499999998',
            'lng' => '107.620796',
            'category_id' => 1,
            'begin_at' => Carbon::createFromFormat('Y-m-d H:i:s', '2018-01-08 8:00:00'),
            'end_at' => Carbon::createFromFormat('Y-m-d H:i:s', '2018-01-08 17:00:00'),
            'user_id' => 1,
        ]);

        Storage::put('class/qowADQnk.md', 'Wujudkan gagasanmu untuk Indonesia, ikuti acara diskusi dan gerakan nyata');
        Storage::put('class/4JBnVBe7.md', 'Wujudkan Gagasanmu untuk Indonesia.');
    }
}
