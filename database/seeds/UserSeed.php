<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $role_employee = Role::where('name', 'Super Admin')->first();

        $employee = new User();
        $employee->name = 'Admin CLS';
        $employee->organization = 'Co-Learning Space';
        $employee->email = 'developwebcls@gmail.com';
        $employee->email_verified_at = '2018-08-01 00:00:00';
        $employee->password = bcrypt('developwebcls271217');
        $employee->handphone = '08123123123';
        $employee->save();
        $employee->role()->associate($role_employee);
        $employee->save();
    }
}
