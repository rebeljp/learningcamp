<?php

use App\Classroom;
use App\Event;
use App\Tag;
use Illuminate\Database\Seeder;

class TagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        Tag::create([
            'name' => 'CLS',
        ]);

        Tag::create([
            'name' => 'Bandung',
        ]);

        Tag::create([
            'name' => 'STUCO',
        ]);

        Tag::create([
            'name' => 'Labtek',
        ]);

        Tag::create([
            'name' => 'Seminar',
        ]);

        Tag::create([
            'name' => 'Open Education',
        ]);

        $event = Classroom::find(1);
        $event->tags()->attach([1, 2, 3]);

        $event2 = Classroom::find(2);
        $event2->tags()->attach([1, 2, 4]);

        $event3 = Event::find(1);
        $event3->tags()->attach([1, 2, 5, 6]);
    }
}
