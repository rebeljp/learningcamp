<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class EventSeed extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        DB::table('events')->insert([
            'name' => 'Bandung Open Education Day',
            'place' => 'Regional Secretariat Bandung',
            'lat' => '-6.9131982',
            'lng' => '107.6090273',
            'category_id' => 1,
            'begin_at' => Carbon::createFromFormat('Y-m-d H:i:s', '2017-08-20 8:00:00'),
            'end_at' => Carbon::createFromFormat('Y-m-d H:i:s', '2017-08-20 17:00:00'),
            'user_id' => 1,
        ]);

        Storage::put('event/M7VKrbDo.md', 'Bandung Open Education Day');
    }
}
