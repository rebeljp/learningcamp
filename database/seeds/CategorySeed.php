<?php

use App\Category;
use Illuminate\Database\Seeder;

class CategorySeed extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        Category::create([
            'name' => 'Teknologi',
            'description' => '',
        ]);
        Category::create([
            'name' => 'Matematik',
            'description' => '',
        ]);
        Category::create([
            'name' => 'Fisika',
            'description' => '',
        ]);
        Category::create([
            'name' => 'Biologi',
            'description' => '',
        ]);
        Category::create([
            'name' => 'Bahasa Indonesia',
            'description' => '',
        ]);
    }
}
