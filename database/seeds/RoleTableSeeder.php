<?php

use App\Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $role_employee = new Role();
        $role_employee->name = 'Super Admin';
        $role_employee->description = 'Super Saiyan';
        $role_employee->save();

        $role_manager = new Role();
        $role_manager->name = 'Admin';
        $role_manager->description = 'Administrator';
        $role_manager->save();

        $role_manager = new Role();
        $role_manager->name = 'Venue';
        $role_manager->description = 'Venue Owner';
        $role_manager->save();

        $role_manager = new Role();
        $role_manager->name = 'Trainer';
        $role_manager->description = 'Trainer';
        $role_manager->save();

        $role_manager = new Role();
        $role_manager->name = 'Trainee';
        $role_manager->description = 'Trainee';
        $role_manager->save();
    }
}
