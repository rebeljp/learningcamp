<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['web']], function () {
    Auth::routes(['verify' => true]);
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/contact-us', 'HomeController@contact')->name('contact');
    Route::get('/about-us', 'HomeController@about')->name('about');
    Route::get('/privacy-policy', 'HomeController@privacy')->name('privacy');
    Route::get('/terms-of-use', 'HomeController@terms')->name('terms');
    Route::get('/data-protection-and-use', 'HomeController@gdpr')->name('gdpr');
    Route::get('/worker', 'HomeController@worker')->name('work');
    Route::get('/web', 'HomeController@webc')->name('web');

    //perlu authentikasi
    Route::group(['middleware' => ['auth']], function () {

        Route::get('logout', 'Auth\LoginController@logout');
        Route::group(['middleware' => ['verified']], function () {
            Route::prefix('admin')->group(function () {
                Route::get('/', 'AdminController@index')->name('admin');
                Route::resource('category', 'CategoryController');
                Route::get('class', 'AdminController@class')->name('manage:class');
                Route::get('event', 'AdminController@event')->name('manage:event');
            });
            Route::get('creator', function () {
                // Matches The "/creator" URL
            });

            Route::resource('tags', 'TagController')->except(['index','show']);
            Route::resource('event', 'EventController')->except(['index','show']);
            Route::resource('class', 'ClassroomController')->except(['index','show']);
            Route::resource('user', 'UserController')->only([
                'edit', 'update'
            ]);
            Route::get('/user/{id}/profile', 'ProfileInvoker')->name('user:profile');
        });
    });
    Route::get('/login/facebook', 'SocialiteController@redirectToFacebookProvider');
    Route::get('/login/facebook/callback', 'SocialiteController@handleProviderFacebookCallback')->name('authFacebookCallback');
    Route::prefix('fb')->group(function () {
        Route::post('/deauthorize', 'SocialiteController@deAuthorize');
    });

    Route::get('/tags', 'TagController@index'); //declare index secara terpisah
    Route::get('/event', 'EventController@index'); //declare index secara terpisah
    Route::get('/class', 'ClassroomController@index'); //declare index secara terpisah

    Route::get('/tags/event/{name}', 'TagController@show'); //declare widlcard diakhir
    Route::get('/tags/class/{name}', 'TagController@show'); //declare widlcard diakhir
    Route::get('/tags/{name}', 'TagController@show'); //declare widlcard diakhir
    Route::get('/event/{id}/{name?}', 'EventController@show'); //declare widlcard diakhir
    Route::get('/class/{id}/{name?}', 'ClassroomController@show'); //declare widlcard diakhir
});
